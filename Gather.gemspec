# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
#require 'Gather/version'
require 'Gather/check'

Gem::Specification.new do |spec|
  spec.name          = "Gather"
  spec.version       = Gather::VERSION
  spec.authors       = ["natedat"]
  spec.email         = ["foilo@ich.com"]
  spec.summary       = %q{ MSF RPC Client}
  spec.description   = %q{ Nothing special.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "scrapi", ">= 0"
  spec.add_development_dependency "mechanize", ">= 0"
  spec.add_development_dependency "tire", ">= 0"
  spec.add_development_dependency "hash", ">= 1.0.0"
  spec.add_development_dependency "development", ">= 1.0.0"
  spec.add_development_dependency "msfrpc-client", ">= 0"
  spec.add_development_dependency "restclient", ">= 0"
  spec.add_development_dependency "text-table", ">= 0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "thread", ">= 0"
end
