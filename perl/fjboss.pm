
package fjboss;

use WWW::Mechanize;
use LWP::UserAgent;
use Carp;

use Data::Dumper;
    
my $Debugging = 0;
my $FakeUserAgent = 'Windows IE 6';$FakeUserAgent = 'Windows Mozilla';$FakeUserAgent = 'Mac Safari';$FakeUserAgent = 'Mac Mozilla';$FakeUserAgent = 'Linux Mozilla';$FakeUserAgent = 'Linux Konqueror';
my @proxylist = ();
my $ipcheck = 'https://www-cgi.tu-darmstadt.de/ip.php';


    sub new {
         my $class = shift;
         my $self = {
            get => [],
        };
        bless($self, $class);
        return $self;
    }
	
    sub debug {
        my $self = shift;
        confess "usage: fget->debug(level)"    unless @_ == 1;

        if (ref($self))  {
            $self->{"_DEBUG"} = $level;         # just myself
        } else {
            $Debugging        = $level;         # whole class
        }
    }
    sub DESTROY {
        my $self = shift;
        if ($Debugging || $self->{"_DEBUG"}) {
            carp "Destroying $self " . $self->name;
        }
    }    

    
#print fget->get('https://www-cgi.tu-darmstadt.de/ip.php','regex=([0-9.]+)<\/p>',[%uahash]);
sub get(){
        my $self = shift;
        confess "usage: fget->get(url,option)"    unless @_ => 1;
        my ($url,$option,$ua) = @_;
 
			if($ua)
			{
				print Dumper($ua);
			}
        
        if($option =~s/regex=//i)
        { 
           # print "[X] GET Regex Selectet:$option\n"; 
	    my $mech = WWW::Mechanize->new(autocheck => 1,timeout => 10);
	    $mech->agent_alias($FakeUserAgent) if $FakeUserAgent;
	 
	    if($mech->get($url))
	    {
	    if($mech->content() =~/$option/i)
	    	{
				$self->{'success'} = 1;
				if($1){ return($1);}
				return(1);
	   	   
	    	}		
	    	else{
				$self->{'success'} = 0;

				return;
			
			}
             }
        }
        else
        {
	    #print "Get @_[0]\n";

	    my $mech = WWW::Mechanize->new(autocheck => 0, timeout => 10);
	    $mech->agent_alias($FakeUserAgent) if $FakeUserAgent;
	    $mech->get($url);
	    return($mech->content());
	}
}

sub post()
{
        my $self = shift;
        confess "usage: fget->get(url,postdata,[option])"    unless @_ => 2;
        my ($url,$postdata,$option) = @_;
        
	    my $ua=LWP::UserAgent->new();
	    $ua->timeout($m_timeout);
	    my $res = $ua->post($url,$postdata);
	    if($res->is_success())
	    {
		my $html=$res->as_string;
		print $html;
		if($option =~s/regex=//i && $html=~/$option/i)
		 {
		   print "Regex: $option\n";
		   print "--> FOUND $1\n";
		   return($1);
		 }
		return(1);
			
	    }else{ return;}
    
}



sub check($$)
{
        my $self = shift;
        confess "usage: fjboss->check(host,[option])"    unless @_ => 1;
        my ($host,$option) = @_;
		

		$return = $self->get('http://'.$host.'/status?full=true','');
		
		
#><h1>Application list</h1><p><a href="#0.0">localhost/</a><br><a href="#1.0">localhost/web-console</a><br><a href="#2.0">localhost/jmx-console</a><br><a href="#3.0">localhost/invoker</a><br><a href="#4.0">localhost/jbossws</a></p><a class="A.name" name="0.0"><h1>localhost/</h1></a><p>
		my @ret = &checkStatus($1,$host) if $return=~/<h1>Application list(.*)<h1>localhost\/<\/h1><\/a><p>/ig;
	#-		print $return."\n";
    
		return

}

sub checkStatus(){
	my $return = shift;
	my ($host) =@_;
		my @arr = split(/<a href=\"#\d+.0\">/,$return);
		
		foreach(@arr){
				next if $_=~/(jbossmq\-httpil|jbossws|invoker|jmx-console|web-console)/;
				print "[X] JSP Apps on $host\t$1\n" if $_=~/[a-z0-9\-\.]+\/([a-z0-9\-\.]+)<\/a></;
				push(@ret,"$host\/$1") if ($1);
	
		}
	
	
		return @ret;

}
1;