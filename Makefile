APP=slave
all: $(APP)
JRUBYJAR="libs/jruby.jar"
JAVA_JAR="jar"
JAVA_FUNC="javapayload.builder.Injector"

all: $(APP)

makejar:
	@cp $(JRUBYJAR) $(APP).jar
	@ls -lah $(APP).jar
	@echo "..."
	@$(JAVA_JAR) -uf $(APP).jar -C classes .
	@ls -lah $(APP).jar
	@echo "Packing done ..."


slave: makejar
	@@$(JAVA_JAR) -ufe $(APP).jar
	@ls -lah $(APP).jar
	@echo "Done..."
	
clean: 
	rm -fR $(APP)*.jar *.tmp




